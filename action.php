<?php
/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 06.03.2017
 * Time: 9:33
 */
ini_set('log_errors', 'On');
ini_set('error_log', '/var/log/php_errors.log');

    session_start();

    require('Router.php');

    $router = new Router();

    switch($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $response = $router->routeRequest($_GET);
            break;
        case 'POST':
            $response = $router->routeRequest($_POST);
            break;
        default:
            $response = false;
            break;
    }

    echo $response;

?>