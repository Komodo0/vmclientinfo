<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="shortcut icon" href="../mainSource/favicon.ico" type="image/x-icon" />

    <script src="../mainSource/jquery-2.2.4/jquery.min.js"></script>

    <script src="../mainSource/moment/moment-with-locales.min.js"></script>

    <script src="../mainSource/bootstrap-3.3.7/js/bootstrap.min.js"></script>

    <script src="../mainSource/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <link href="../mainSource/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="../mainSource/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/i18n/defaults-ru_RU.min.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.format.js"></script>

    <script src="../mainSource/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>

    <script language="javascript" type="text/javascript" src="scripts/alasql/console/alasql.min.js"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/amcharts.js" type="text/javascript"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/pie.js" type="text/javascript"></script>

<!--    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">-->

    <script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" type="text/css" href="style/style.css">

    <title>
        RitmTools
    </title>

</head>
<body style="height: 100%;">

<a href="index" style="position: absolute; font-size: 30px; padding-left:4px; padding-right:8px; margin-top:15px; margin-left:10px" class="btn btn-default">
    <span class="glyphicon glyphicon-home" aria-hidden="true" style="width:35px;"></span>
</a>

    <div class="container" style="height: 100%;">
        <div class="row" style="height: 10%; border-left:1px solid #ccc; border-right:1px solid #ccc;">

            <div class="col-lg-3 col-md-3" style="text-align:center;">
                <div class="input-group" style="width:70%; display: inline-block; margin-top:20px;">
                    <select id="timestampsList" name="timestamp_id" class="form-control"></select>
                </div>
                <div class="input-group" style="width:20%; display: inline-block;">
                    <button id="buildDiagramms" class="form-control btn btn-default"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></button>
                </div>
            </div>

            <div class="col-lg-6 col-md-6" style="">
                <h1 style="text-align: center">Использование системных ресурсов</h1>
            </div>

            <div class="col-lg-3 col-md-3" style="text-align:center;">
               <!-- <div class="input-group" style="width:100%; display: inline-block; margin-top:20px;">
                    <button class="form-control btn btn-default">Загрузить CSV<span class="glyphicon glyphicon-arrow-up pull-right" aria-hidden="true"></span></button>
                </div>-->
            </div>

        </div>

        <div class="row" style="height: 90%;  border-left:1px solid #ccc; border-right:1px solid #ccc;">

            <div class="col-lg-12 col-md-12" style="height: 100%; overflow-y: hidden;">
                <div style="height: 10%;">
                    <ul class="nav nav-tabs nav-justified">
                        <li id="storageTab" class="active"><a href="#">Использование DataStore</a></li>
                        <li id="hostTab"><a href="#">Использование CPU+RAM</a></li>
                    </ul>
                </div>
                <div style="height: 90%; overflow-x: hidden; overflow-y: scroll;">
                    <div class="loader loading">
                        <!--<div class="loader_animation">
                            <img src="static/2.gif">
                        </div>-->
                    </div>
                    <div class="diagramm_container">
                        <!---      Сюда кидаем графики стораджей    --->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    ?>





    <script src="scripts/script.js"></script>
    <script src="scripts/index.js"></script>
</body>
</html>