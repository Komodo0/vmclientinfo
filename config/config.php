<?php
/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 03.03.2017
 * Time: 10:50
 */
function getConfig(){

    $config = array(

        //Наша БД
        'DB_URL' => 'localhost',
        'DB_USERNAME' => 'root',
        'DB_PASSWORD' => 'masterkey',
        'DB_NAME' => 'vm_client_info',
        'DB_PORT' => 3306

    );

    return $config;

}