<?php

/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 06.03.2017
 * Time: 9:37
 */
class Router
{

    public function routeRequest($request){

        if (isset($request) && isset($request['action'])){


            switch ($request['action']){

                case 'getTimestampList':
                    return $this->getTimestampList($request);
                    break;

                case 'getStorageList':
                    return $this->getStorageList($request);
                    break;

                case 'getClientList':
                    return $this->getClientList($request);
                    break;

                case 'getHostList':
                    return $this->getHostList($request);
                    break;

                case 'getVmList':
                    return $this->getVmList($request);
                    break;

                case 'getHistoryByTimestampId':
                    return $this->getHistoryByTimestampId($request);
                    break;

                case 'getSettings':
                    return $this->getSettings($request);
                    break;

                case 'saveConfigSettings':
                    return $this->saveConfigSettings($request);
                    break;

                case 'saveStorageSettings':
                    return $this->saveStorageSettings($request);
                    break;

                case 'saveHostSettings':
                    return $this->saveHostSettings($request);
                    break;

                case 'getStorageHistoryById':
                    return $this->getStorageHistoryById($request);
                    break;

                default:
                    return false;
            }

        } else {
            return false;
        }



    }

    private function getTimestampList($request){

        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        return json_encode(DataExporter::getTimestampListAsArray($connector));
    }

    private function getClientList($request){
        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );


        $result = DataExporter::getClientList($connector);
        return json_encode($result);
    }

    private function getHostList($request){
        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $result = DataExporter::getHostList($connector);
        return json_encode($result);
    }

    private function getStorageList($request){
        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $result = DataExporter::getStorageList($connector);

        return json_encode($result);
    }

    private function getVmList($request){
        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $result = DataExporter::getVmList($connector);
        return json_encode($result);
    }

    private function getHistoryByTimestampId($request){

        $timestampId = $request['timestampId'];
        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $result = array(
            'vm_history' => DataExporter::getVmHistoryByTimestampId($connector, $timestampId),
            'storage_history' => DataExporter::getStorageHistoryByTimestampId($connector, $timestampId),
            'host_history' => DataExporter::getHostHistoryByTimestampId($connector, $timestampId),
        );

        return json_encode($result);

    }

    private function getSettings($request){
        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );
        $result = DataExporter::getSettings($connector);
        return json_encode($result);

    }

    private function saveConfigSettings($request){
        require 'classes/DataImporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $settings = json_decode($request['data'], true);

        $result = DataImporter::saveConfigSettings($connector, $settings);
        return $result;
    }

    private function saveStorageSettings($request){
        require 'classes/DataImporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $settings = json_decode($request['data'], true);

        $result = DataImporter::saveStorageSettings($connector, $settings);
        return $result;
    }

    private function saveHostSettings($request){
        require 'classes/DataImporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );

        $settings = json_decode($request['data'], true);

        $result = DataImporter::saveHostSettings($connector, $settings);
        return $result;
    }

    private function getStorageHistoryById($request){

        $storageId = $request['storageId'];

        require 'classes/DataExporter.php';
        require 'config/config.php';
        $config = getConfig();
        require 'classes/DBconnector.php';
        $connector = new DBconnector(
            $config['DB_URL'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            $config['DB_NAME'],
            $config['DB_PORT']
        );
        $result = DataExporter::getStorageHistoryById($connector, $storageId);
        return json_encode($result);
    }
}