<?php
/**
 * Это будет запускаться по расписанию крон и работать с файлами.
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 26.04.2017
 * Time: 14:27
 */
ini_set('log_errors', 'On');
ini_set('error_log', '/var/log/php_errors.log');

function cleanDir($path) {
    if (file_exists($path)){
        foreach (glob($path.'*') as $file){
            unlink($file);
        }
    }
}



    ini_set('memory_limit', '-1');
    set_time_limit(600);
    require_once 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

    $app_name = 'VmClientInfo_dev';
    $tmp_path = $_SERVER['DOCUMENT_ROOT'].'/'.$app_name.'/tmp/';


    require $_SERVER['DOCUMENT_ROOT'].'/'.$app_name.'/config/config.php';
    $config = getConfig();
    require $_SERVER['DOCUMENT_ROOT'].'/'.$app_name.'/classes/DBconnector.php';
    $connector = new DBconnector(
        $config['DB_URL'],
        $config['DB_USERNAME'],
        $config['DB_PASSWORD'],
        $config['DB_NAME'],
        $config['DB_PORT']
    );

    $query = "SELECT value AS path FROM config c WHERE parameter = 'input_file_path'";

    $files_path = $connector->executeScript($query)[0]['path'];

    $file_names_list = array_diff(scandir($files_path), array('..', '.'));

    $file_dates_list = array();

    foreach ($file_names_list as $index => $file_name){
        cleanDir($tmp_path);
        $pattenr = '/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9].[0-9][0-9].[0-9][0-9]/';
        $matches = array();
        $datetime_path = preg_match($pattenr, $file_name, $matches);
        $timestamp = DateTime::createFromFormat ('Y-m-d_H.i.s', $matches[0])->format('Y-m-d H:i:s');

        $query = "SELECT timestamp FROM `timestamp` t WHERE timestamp = '$timestamp'";

        $result = $connector->executeScript($query);

        if (!count($result)){

            //ТУТ ПАРСИМ ФАЙЛ

            //$file_name - имя файла
            //$timestamp - таймстамп который пишем в базу
            cleanDir($tmp_path);

            //Конвертируем xlsx в кучу CSV

            $inputFileType = 'Excel2007';
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcelReader = $objReader->load($files_path . $file_name);
            $loadedSheetNames = $objPHPExcelReader->getSheetNames();
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');
            foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                $objWriter->setSheetIndex($sheetIndex);
                $objWriter->setDelimiter(';');
                $objWriter->save($tmp_path.$loadedSheetName.'.csv');
            }





            //Импортируем из CSV

            $timestampName = '';
            $hostPath = $tmp_path . 'vHost.csv';
            $storagePath = $tmp_path . 'vDatastore.csv';
            $vmPath = $tmp_path . 'vInfo.csv';


            echo $hostPath.'<br>'.$storagePath.'<br>'.$vmPath.'<br>';


            require_once $_SERVER['DOCUMENT_ROOT'].'/'.$app_name.'/classes/CsvParser.php';
            $vmFormattedArray = CsvParser::getVmFormattedArray(CsvParser::getCsvFileContent($vmPath));
            $hostFormattedArray = CsvParser::getHostFormattedArray(CsvParser::getCsvFileContent($hostPath));
            $storageFormattedArray = CsvParser::getStorageFormattedArray(CsvParser::getCsvFileContent($storagePath));
            require_once $_SERVER['DOCUMENT_ROOT'].'/'.$app_name.'/classes/DataImporter.php';
            DataImporter::importDataFromFormattedArrays($connector, $timestamp, $timestampName, $vmFormattedArray, $hostFormattedArray, $storageFormattedArray);



            echo '<pre>';
                var_dump(CsvParser::getCsvFileContent($vmPath));
            echo '</pre>';
            echo '<pre>';
            var_dump(CsvParser::getCsvFileContent($hostPath));
            echo '</pre>';
        }
    }




?>