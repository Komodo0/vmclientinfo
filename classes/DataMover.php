<?php

/**
 * Created by PhpStorm.
 * User: Komodo
 * Date: 09.03.2017
 * Time: 23:58
 */
class DataMover
{

    public static function getVmHistoryByTimestampId($connector, $timestampId){

        $timestampId = intval($timestampId);

        $query = "SELECT
                      id, vm_id, timestamp_id, storage_id, host_id, power_state, cpu, ram, privisioned_disk_space, used_disk_space
                    FROM
                      vm_history v
                    WHERE
                      v.timestamp_id = '$timestampId' AND power_state = 1
                    ";

        $result = $connector->executeScript($query);

        return $result;

    }

    public static function getStorageHistoryByTimestampId($connector, $timestampId){

        $timestampId = intval($timestampId);

        $query = "SELECT
                      id, timestamp_id, storage_id, disk_space
                    FROM
                      storage_history s
                    WHERE
                      s.timestamp_id = '$timestampId'
                    ";

        $result = $connector->executeScript($query);

        return $result;

    }

    public static function getHostHistoryByTimestampId($connector, $timestampId){

        $timestampId = intval($timestampId);

        $query = "SELECT
                      id, timestamp_id, host_id, cpu_usage, vcpus, memory_usage
                    FROM
                      host_history h
                    WHERE
                      h.timestamp_id = '$timestampId'
                    ";

        $result = $connector->executeScript($query);

        return $result;

    }

    /////////////////////////////////////////////////////////////////////////

    public static function getTimestampsList($connector){

        $query = "SELECT id, name, timestamp, display FROM timestamp t";

        $result = $connector->executeScript($query);

        return $result;

    }

    public static function createTimestamp($connector, $timestamp, $name){

        $name   = addslashes($name);

        $query = "INSERT INTO timestamp (timestamp, name) VALUES ('$timestamp', '$name');";

        $result = $connector->executeInsertScript($query);

        return $result;

    }

    /////////////////////////////////////////////////////////////////////////

    public static function getHostIdByHost($connector, $host){

        $host = addslashes($host);

        $query = "SELECT id FROM host h where h.host = '$host';";

        $result = $connector->executeScript($query);

        return intval($result[0]['id']);

    }

    public static function createHost($connector, $host, $cores, $memory){

        $host   = addslashes($host);
        $cores = intval($cores);
        $memory = intval($memory);

        $query = "INSERT INTO host (host, cores, memory) VALUES ('$host', '$cores', '$memory');";

        $result = $connector->executeInsertScript($query);

        return $result;

    }

    public static function createHostHistoryNote($connector, $timestampId, $hostId, $cpuUsage, $vCPUs, $memoryUsage){

        $timestampId = intval($timestampId);
        $hostId = intval($hostId);
        $cpuUsage = intval($cpuUsage);
        $vCPUs = intval($vCPUs);
        $memoryUsage = intval($memoryUsage);

        $query = "
          INSERT INTO
            host_history (timestamp_id, host_id, cpu_usage, vcpus, memory_usage)
          VALUES
            ('$timestampId', '$hostId', '$cpuUsage', '$vCPUs', '$memoryUsage');
          ";
        $result = $connector->executeInsertScript($query);
        return $result;

    }

    /////////////////////////////////////////////////////////////////////////

    public static function getStorageIdByName($connector, $name){

        $name = addslashes($name);

        $query = "SELECT id FROM storage s where s.name = '$name';";

        $result = $connector->executeScript($query);

        return intval($result[0]['id']);

    }

    public static function  createStorage($connector, $name, $alias = null){

        $name   = addslashes($name);

        if ($alias == null){
            $alias = $name;
        } else {
            $alias = addslashes($alias);
        }



        $query = "INSERT INTO storage (name, alias) VALUES ('$name', '$alias');";

        $result = $connector->executeInsertScript($query);

        return $result;

    }

    public static function createStorageHistoryNote($connector, $timestampId, $storageId, $diskSpace){

        $timestampId = intval($timestampId);
        $storageId = intval($storageId);
        $diskSpace = intval($diskSpace);

        $query = "
          INSERT INTO
            storage_history (timestamp_id, storage_id, disk_space)
          VALUES
            ('$timestampId', '$storageId', '$diskSpace');
          ";
        $result = $connector->executeInsertScript($query);
        return $result;

    }

    /////////////////////////////////////////////////////////////////////////

    public static function getClientIdByName($connector, $name){

        $name = addslashes($name);

        $query = "SELECT id FROM client c where c.name = '$name';";

        $result = $connector->executeScript($query);

        return intval($result[0]['id']);

    }

    public static function createClient($connector, $name){

        $name   = addslashes($name);

        $query = "INSERT INTO client (name) VALUES ('$name');";

        $result = $connector->executeInsertScript($query);

        return $result;

    }

    /////////////////////////////////////////////////////////////////////////

    public static function getVmIdByName($connector, $name){

        $name = addslashes($name);

        $query = "SELECT id FROM vm v where v.name = '$name';";

        $result = $connector->executeScript($query);

        return intval($result[0]['id']);

    }

    public static function createVm($connector, $clientId, $name, $dnsName, $vmId){

        $name   = addslashes($name);
        $clientId = intval($clientId);
        $vmId = addslashes($vmId);
        $dnsName = addslashes($dnsName);

        $query = "
          INSERT INTO
            vm (client_id, name, dns_name, vm_id)
          VALUES
            ('$clientId', '$name', '$dnsName', '$vmId');
          ";

        $result = $connector->executeInsertScript($query);

        return $result;
    }

    public static function createVmHistoryNote($connector, $vmId, $timestampId, $storageId, $hostId, $powerState, $cpu, $ram, $privisionedDiskSpace, $usedDiskSpace){

        $vmId = intval($vmId);
        $timestampId = intval($timestampId);
        $storageId = intval($storageId);
        $hostId = intval($hostId);
        $powerState = intval($powerState);
        $cpu = intval($cpu);
        $ram = intval($ram);
        $privisionedDiskSpace = intval($privisionedDiskSpace);
        $usedDiskSpace = intval($usedDiskSpace);

        $query = "
          INSERT INTO
            vm_history (vm_id, timestamp_id, storage_id, host_id, power_state, cpu, ram, privisioned_disk_space, used_disk_space)
          VALUES
            ('$vmId', '$timestampId', '$storageId', '$hostId', '$powerState', '$cpu', '$ram', '$privisionedDiskSpace', '$usedDiskSpace');
          ";
        $result = $connector->executeInsertScript($query);
        return $result;
    }

    /////////////////////////////////////////////////////////////////////////

    public static function getVmList($connector){
        $query = "SELECT id, client_id, name, dns_name, vm_id FROM vm v;";
        $result = $connector->executeScript($query);
        return $result;
    }

    public static function getClientList($connector){
        $query = "SELECT id, name FROM client c;";
        $result = $connector->executeScript($query);
        return $result;

    }

    public static function getStorageList($connector){
        $query = "SELECT id, name, alias, display FROM storage s;";
        $result = $connector->executeScript($query);
        return $result;
    }

    public static function getHostList($connector){
        $query = "SELECT id, host, cores, memory, display FROM host h;";
        $result = $connector->executeScript($query);
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////
    public static function getSettings($connector){
        $query = "SELECT id, parameter, value FROM config c;";
        $result = $connector->executeScript($query);
        return $result;
    }

    public static function saveConfigSettings($connector, $settings){
        foreach ($settings as $data){
            $id = intval($data['name']);
            $value = intval($data['value']);
            $query = "UPDATE config SET value = '$value' WHERE id = '$id';";
            $connector->executeScript($query);
        }

        return 1;
    }

    public static function saveStorageSettings($connector, $settings){
        foreach ($settings as $data){
            $id = intval($data['id']);
            $alias = $data['alias'];
            $display = intval($data['display']);
            $query = "UPDATE storage s SET display = '$display' , alias = '$alias' WHERE s.id = '$id';";
            $connector->executeScript($query);
        }
        return 1;
    }

    public static function saveHostSettings($connector, $settings){

        $query = "UPDATE host h SET display = 0;";
        $connector->executeScript($query);
        foreach ($settings as $data){
            $id = intval($data['id']);
            $display = intval($data['display']);
            $query = "UPDATE host h SET display = '$display' WHERE h.id = '$id';";
            $connector->executeScript($query);
        }
        return 1;
    }

    public static function getStorageHistoryById($connector, $storageId)
    {
        $storageId = intval($storageId);
        if ($storageId == -1) {
            $query = "
                SELECT 
                  jn.timestamp_id, 
                  ts.timestamp, 
                  jn.client_id, 
                  jn.used_disk_space 
                FROM 
                  (SELECT 
                    timestamp_id, 
                    vm.client_id, 
                    SUM(used_disk_space) AS used_disk_space 
                  FROM 
                    vm_history vh 
                  INNER JOIN 
                    vm vm 
                  ON 
                    vm.id = vh.vm_id 
                  GROUP BY 
                    timestamp_id, vm.client_id
                  ) jn 
                INNER JOIN 
                  timestamp ts 
                ON 
                  ts.id = jn.timestamp_id 
            ";
        } else {
            $query = "
                SELECT 
                  jn.timestamp_id, 
                  ts.timestamp, 
                  jn.client_id, 
                  jn.used_disk_space 
                FROM 
                  (SELECT 
                    timestamp_id, 
                    vm.client_id, 
                    SUM(used_disk_space) AS used_disk_space 
                  FROM 
                    vm_history vh 
                  INNER JOIN 
                    vm vm 
                  ON 
                    vm.id = vh.vm_id 
                  WHERE 
                    storage_id = '$storageId' 
                  GROUP BY 
                    timestamp_id, vm.client_id
                  ) jn 
                INNER JOIN 
                  timestamp ts 
                ON 
                  ts.id = jn.timestamp_id 
            ";
        }
        $result = $connector->executeScript($query);
        return $result;
    }

}