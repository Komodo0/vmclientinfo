<?php

/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 10.03.2017
 * Time: 13:09
 */
class DataExporter
{

    public static function getTimestampListAsArray($connector){
        include_once  'DataMover.php';
        return DataMover::getTimestampsList($connector);
    }

    public static function getVmList($connector){
        include_once  'DataMover.php';
        return DataMover::getVmList($connector);
    }

    public static function getClientList($connector){
        include_once  'DataMover.php';
        return DataMover::getClientList($connector);
    }

    public static function getStorageList($connector){
        include_once  'DataMover.php';
        return DataMover::getStorageList($connector);
    }

    public static function getHostList($connector){
        include_once  'DataMover.php';
        return DataMover::getHostList($connector);
    }

    public static function getVmHistoryByTimestampId($connector, $timestampId){
        include_once  'DataMover.php';
        return DataMover::getVmHistoryByTimestampId($connector, $timestampId);
    }

    public static function getStorageHistoryByTimestampId($connector, $timestampId){
        include_once  'DataMover.php';
        return DataMover::getStorageHistoryByTimestampId($connector, $timestampId);
    }

    public static function getHostHistoryByTimestampId($connector, $timestampId){
        include_once  'DataMover.php';
        return DataMover::getHostHistoryByTimestampId($connector, $timestampId);
    }

    public static function getSettings($connector){
        include_once  'DataMover.php';
        return DataMover::getSettings($connector);
    }

    public static function getStorageHistoryById($connector, $storageId){
        include_once  'DataMover.php';
        return DataMover::getStorageHistoryById($connector, $storageId);
    }

}