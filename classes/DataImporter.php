<?php

/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 06.03.2017
 * Time: 16:16
 */
class DataImporter
{

    public static function saveConfigSettings($DBconnector, $settings){
        require 'DataMover.php';
        $result = DataMover::saveConfigSettings($DBconnector, $settings);
        return $result;
    }

    public static function saveStorageSettings($DBconnector, $settings){
        require 'DataMover.php';
        $result = DataMover::saveStorageSettings($DBconnector, $settings);
        return $result;
    }

    public static function saveHostSettings($DBconnector, $settings){
        require 'DataMover.php';
        $result = DataMover::saveHostSettings($DBconnector, $settings);
        return $result;
    }

    public static function importDataFromFormattedArrays($DBconnector, $timestamp, $timestampName, $vmFormattedArray, $hostFormattedArray /* $clusterFormattedArray */, $storageFormattedArray){

        require 'DataMover.php';

        //$resultArray = array();

        $timestampId = DataMover::createTimestamp($DBconnector, $timestamp, $timestampName);

//        //Заносим, создаем кластеры и кластер-таймстампы
//        foreach ($clusterFormattedArray as $note){
//            $clusterId = DataMover::getClusterIdByName($DBconnector, $note['name']);
//            if (!$clusterId){
//                $clusterId = DataMover::createCluster($DBconnector, $note['name']);
//            }
//            $clusterHistoryId = DataMover::createClusterHistoryNote(
//                $DBconnector,
//                $timestampId,
//                $clusterId,
//                $note['cpu'],
//                $note['ram']
//            );
//        }

        //Заносим и создаем Хосты и Хост-таймстампы
        foreach ($hostFormattedArray as $note){
            $hostId = DataMover::getHostIdByHost($DBconnector, $note['host']);
            if (!$hostId){
                $hostId = DataMover::createHost(
                    $DBconnector,
                    $note['host'],
                    $note['cores'],
                    $note['memory']
                );
            }
            $hostHistoryId = DataMover::createHostHistoryNote(
                $DBconnector,
                $timestampId,
                $hostId,
                $note['cpu_usage'],
                $note['vcpus'],
                $note['memory_usage']
            );
        }


        //Заносим, создаем стораджи и сторадж-таймстампы
        foreach ($storageFormattedArray as $note){
            $storageId = DataMover::getStorageIdByName($DBconnector, $note['name']);
            if (!$storageId){
                $storageId = DataMover::createStorage($DBconnector, $note['name']);
            }
            $storageHistoryId = DataMover::createStorageHistoryNote(
                $DBconnector,
                $timestampId,
                $storageId,
                $note['disk_space']
            );
        }

        ///////////
        foreach ($vmFormattedArray as $note){

            $clientId = DataMover::getClientIdByName($DBconnector, $note['client_name']);

            if (!$clientId){
                $clientId = DataMover::createClient($DBconnector, $note['client_name']);
            }

            $vmId = DataMover::getVmIdByName($DBconnector, $note['vm_name']);
            if (!$vmId){
                $vmId = DataMover::createVm($DBconnector,
                    $clientId,
                    $note['vm_name'],
                    $note['dns_name'],
                    $note['id']);
            }

            $storageId = DataMover::getStorageIdByName($DBconnector, $note['storage']);

            $hostId = DataMover::getHostIdByHost($DBconnector, $note['host']);

            $historyId = DataMover::createVmHistoryNote(
                $DBconnector,
                $vmId,
                $timestampId,
                $storageId,
                $hostId,
                $note['powerstate'],
                $note['cpu'],
                $note['ram'],
                $note['provisioned_disk_space'],
                $note['used_disk_space']);
        }

        //Надо что-то вернуть, но пока не ясно что.
        //Пока что служебная утилита
        //return null;
    }

}