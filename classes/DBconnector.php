<?php 

class DBconnector{

	private $db_ip;
	private $db_login;
	private $db_password;
	private $db_name;
	private $db_port;
	
	function __construct($db_ip = 'localhost', $db_login ='', $db_password = '', $db_name = '', $db_port = 3306){
		$this->db_ip = $db_ip;
		$this->db_login = $db_login;
		$this->db_password = $db_password;
		$this->db_name = $db_name;
		$this->db_port = $db_port;
	}
   
	function getIp(){
		return $this->db_ip;
	}

	function connectionIsOk(){
		$connection = new mysqli($this->db_ip, $this->db_login, $this->db_password, $this->db_name, $this->db_port);
        if ($connection->connect_errno) {
            return false;
        } else {
			return true;
		}
	}

	function getMysqliLink(){
	    return new mysqli($this->db_ip, $this->db_login, $this->db_password, $this->db_name, $this->db_port);
    }

	function executeScript($request){
		if ($this->connectionIsOk()){
			$result = false;
			$connection = new mysqli($this->db_ip, $this->db_login, $this->db_password, $this->db_name, $this->db_port);
            $connection->set_charset("utf8");
			$query = $connection->query($request);
			if ($query){
				$num_rows = $query->num_rows;
				$result = array();
				for ($i = 0; $i < $num_rows; $i++){
					$result[$i] = $query->fetch_array(MYSQLI_ASSOC);
				}
				//$query->close();
				return $result;
			}
			return $connection->info;
		} else {
			return 'Connection Is Not Ok!';
		}
	}


	function executeInsertScript($request){
		if ($this->connectionIsOk()){
			$connection = new mysqli($this->db_ip, $this->db_login, $this->db_password, $this->db_name, $this->db_port);
            $connection->set_charset("utf8");
			$result = $connection->query($request);
			if ($result){
                return $connection->insert_id;
            } else {
			    return 'Query failed!';
            }

		} else {
			return 'Connection Is Not Ok!';
		}
	}

}
?>