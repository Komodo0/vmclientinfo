<?php

/**
 * Created by PhpStorm.
 * User: i.kotelnikov
 * Date: 06.03.2017
 * Time: 10:45
 */
class CsvParser
{

    //Возвращает массив с массивами данных строк
    public static function getCsvFileContent($path){
        $content = array();
        $handle = fopen('php://memory', 'w+');
        fwrite($handle, file_get_contents($path));
        rewind($handle);
        while (($row = fgetcsv($handle, 0, ';')) !== false) {
            array_push($content, $row);
        };
        fclose($handle);
        return $content;
    }

    public static function getVmFormattedArray($unformattedArray){
        //  Нас интересуют столбцы:
        //  VM              - vm.name
        //  Powerstate      - vm.power_state
        //  DNS Name        - vm.dns_name
        //  CPUs            - vm.cpu
        //  Memory          - vm.ram
        //  Host            - vm.host
        //  VM ID           - vm.vm_id
        //  Client          - client.name
        //  Path            - содержит в себе storage.name
        //  Provisioned MB  - vm.provisioned_disk_space
        //  In Use MB       - vm.used_disk_space
        $columnNumbers = array();
        $names = $unformattedArray[0];

        foreach ($names as $colNum => $name){
            switch ($name){
                case 'VM':
                $columnNumbers['name'] = $colNum;
                    break;

                case 'Powerstate':
                    $columnNumbers['powerstate'] = $colNum;
                    break;

                case 'DNS Name':
                    $columnNumbers['dns_name'] = $colNum;
                    break;

                case 'CPUs':
                    $columnNumbers['cpu'] = $colNum;
                    break;

                case 'Memory':
                    $columnNumbers['ram'] = $colNum;
                    break;

                case 'Host':
                    $columnNumbers['host'] = $colNum;
                    break;

                case 'VM ID':
                    $columnNumbers['id'] = $colNum;
                    break;

                case 'Client':
                    $columnNumbers['client_name'] = $colNum;
                    break;

                case 'Path':
                    $columnNumbers['path'] = $colNum;
                    break;

                case 'Provisioned MB':
                    $columnNumbers['provisioned_disk_space'] = $colNum;
                    break;

                case 'In Use MB':
                    $columnNumbers['used_disk_space'] = $colNum;
                    break;
            }
        }

        $formattedArray = array();

        foreach($unformattedArray as $key => $vm){
            if ($key != 0){
                $ram = CsvParser::removeBadSymbols($vm[$columnNumbers['ram']]);
                $powerstate = 0;
                if ($vm[$columnNumbers['powerstate']] == 'poweredOn'){
                    $powerstate = 1;
                }
                $provisioned_disk_space = CsvParser::removeBadSymbols($vm[$columnNumbers['provisioned_disk_space']]);
                $used_disk_space = CsvParser::removeBadSymbols($vm[$columnNumbers['used_disk_space']]);
                array_push($formattedArray, array(
                    'vm_name' => $vm[$columnNumbers['name']],
                    'powerstate' => $powerstate,
                    'dns_name' => $vm[$columnNumbers['dns_name']],
                    'cpu' => $vm[$columnNumbers['cpu']],
                    'ram' => $ram,
                    'host' => $vm[$columnNumbers['host']],
                    'id' => $vm[$columnNumbers['id']],
                    'client_name' => $vm[$columnNumbers['client_name']],
                    'storage' => substr($vm[$columnNumbers['path']], 1, stripos($vm[$columnNumbers['path']], ']')-1),
                    'provisioned_disk_space' => $provisioned_disk_space,
                    'used_disk_space' => $used_disk_space,
                ));
            }
        }

        return $formattedArray;

    }

    public static function getClusterFormattedArray($unformattedArray){
        //  Нас интересуют столбцы:
        //  Name            - cluster.name
        //  TotalCpu        - cluster.total_cpu
        //  TotalMemory     - cluster.total_memory

        $columnNumbers = array();
        $names = $unformattedArray[0];

        foreach ($names as $colNum => $name){
            switch ($name){
                case 'Name':
                    $columnNumbers['name'] = $colNum;
                    break;

                case 'TotalCpu':
                    $columnNumbers['cpu'] = $colNum;
                    break;

                case 'TotalMemory':
                    $columnNumbers['ram'] = $colNum;
                    break;
            }
        }

        $formattedArray = array();

        foreach($unformattedArray as $key => $vm){
            if ($key != 0){
                $cpu = CsvParser::removeBadSymbols($vm[$columnNumbers['cpu']]);
                $ram = CsvParser::removeBadSymbols($vm[$columnNumbers['ram']]);
                array_push($formattedArray, array(
                    'name' => $vm[$columnNumbers['name']],
                    'cpu' => $cpu,
                    'ram' => $ram
                ));
            }
        }

        return $formattedArray;

    }

    public static function getHostFormattedArray($unformattedArray){
        //  Нас интересуют столбцы:
        //  Host            - host.host
        //  # Cores         - host.cores
        //  CPU usage %     - host.cpu_usage
        //  # vCPUs         - host.vcpus
        //  # Memory        - host.memory
        //  Memory usage %  - host.memory_usage


        $columnNumbers = array();
        $names = $unformattedArray[0];

        foreach ($names as $colNum => $name){
            switch ($name){
                case 'Host':
                    $columnNumbers['host'] = $colNum;
                    break;

                case '# Cores':
                    $columnNumbers['cores'] = $colNum;
                    break;

                case 'CPU usage %':
                    $columnNumbers['cpu_usage'] = $colNum;
                    break;

                case '# vCPUs':
                    $columnNumbers['vcpus'] = $colNum;
                    break;

                case '# Memory':
                    $columnNumbers['memory'] = $colNum;
                    break;

                case 'Memory usage %':
                    $columnNumbers['memory_usage'] = $colNum;
                    break;
            }
        }

        $formattedArray = array();

        foreach($unformattedArray as $key => $vm){
            if ($key != 0){
                $host = $vm[$columnNumbers['host']];
                $cores = CsvParser::removeBadSymbols($vm[$columnNumbers['cores']]);
                $cpu_usage = $vm[$columnNumbers['cpu_usage']];
                $vcpus = $vm[$columnNumbers['vcpus']];
                $memory = CsvParser::removeBadSymbols($vm[$columnNumbers['memory']]);
                $memory_usage = $vm[$columnNumbers['memory_usage']];

                array_push($formattedArray, array(
                    'host' => $host,
                    'cores' => $cores,
                    'cpu_usage' => $cpu_usage,
                    'vcpus' => $vcpus,
                    'memory' => $memory,
                    'memory_usage' => $memory_usage,
                ));
            }
        }

        return $formattedArray;

    }

    public static function getStorageFormattedArray($unformattedArray){
        //  Нас интересуют столбцы:
        //  Name            - storage.name
        //  Capacity MB     - storage.disk_space

        $columnNumbers = array();
        $names = $unformattedArray[0];

        foreach ($names as $colNum => $name){
            switch ($name){
                case 'Name':
                    $columnNumbers['name'] = $colNum;
                    break;

                case 'Capacity MB':
                    $columnNumbers['disk_space'] = $colNum;
                    break;
            }
        }

        $formattedArray = array();

        foreach($unformattedArray as $key => $vm){
            if ($key != 0){
                $disk_space = CsvParser::removeBadSymbols($vm[$columnNumbers['disk_space']]);
                array_push($formattedArray, array(
                    'name' => $vm[$columnNumbers['name']],
                    'disk_space' => $disk_space
                ));
            }
        }

        return $formattedArray;

    }

    public static function removeBadSymbols($string){
        $res = str_replace(" ","",$string);
        $res = str_replace(",","",$res);
        return $res;
    }
}