<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="shortcut icon" href="../mainSource/favicon.ico" type="image/x-icon" />

    <script src="../mainSource/jquery-2.2.4/jquery.min.js"></script>

    <script src="../mainSource/moment/moment-with-locales.min.js"></script>

    <script src="../mainSource/bootstrap-3.3.7/js/bootstrap.min.js"></script>

    <script src="../mainSource/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <link href="../mainSource/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="../mainSource/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/i18n/defaults-ru_RU.min.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.format.js"></script>

    <script src="../mainSource/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>

    <script language="javascript" type="text/javascript" src="scripts/alasql/console/alasql.min.js"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/amcharts.js" type="text/javascript"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/serial.js" type="text/javascript"></script>

    <!--    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">-->

    <script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

    <script src="scripts/script.js"></script>

    <link rel="stylesheet" type="text/css" href="style/style.css">

    <title>
        RitmTools
    </title>

</head>
<body style="height: 100%;">
<div class="container" style="text-align: center">
    <h1>Мониторинг клиентской виртуализации</h1>
    <hr>
    <a href="usage_info" class="btn btn-default btn-lg">Использование системных ресурсов</a>
    <hr>
    <a href="storage_stat" class="btn btn-default btn-lg">Статистика использования диска</a>
</div>
</body>
</html>