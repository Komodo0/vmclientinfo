/**
 * Created by i.kotelnikov on 22.05.2017.
 */
//Создает Event-Listener'ы для таблицы
function callTableEventListener(){

    //Реакция на чек-боксы
    $(".diagramm_table > thead > tr:nth-child(2) > th > input[type='checkbox']").on('click', function () {

        if ($(this).is(':checked')){
            checked = 1;
        } else {
            checked = 0;
        }
        client = $(this).val();
        if (client=='main'){
            $(".diagramm_table input[type='checkbox']").prop("checked", checked);
        }

    });

    //Реакция на сортировку
    window.clTable = $('.diagramm_table').DataTable({
        colReorder: true,
        paging:false,
        searching:false,
        bInfo : false
    });
    if (window.clTableOrder.length != 0){
        window.clTable.order(window.clTableOrder).draw();
    }

    $('.diagramm_table > thead > tr:nth-child(2) > th').on('click', function ( e, settings, details ) {
        columnId = window.clTable.order()[0][0];
        if (columnId != 0){
            sort = window.clTable.order()[0];

            window.clTableOrder.forEach(function(item, i, array){
                if (item[0] == columnId){
                    array.splice([i],1);
                }
            });
            window.clTableOrder.unshift(sort);
        }
    } );



    //Костыль, чтоб не сбивались стили таблицы после добавления к ней сортировщика
    $("table").removeAttr("style");
    $("thead > :nth-child(2) > th").removeAttr("style");
    $("thead").css('cursor', 'pointer');
    $(".diagramm_table").removeAttr("style");
    $(".diagramm_table > thead > :nth-child(2) > th").on("click", function () {
        arrowSpan = $(this).find(".glyphicon");
        if (arrowSpan.hasClass("glyphicon-resize-vertical") || arrowSpan.hasClass("glyphicon-sort-by-attributes-alt")){
            newClass = "glyphicon-sort-by-attributes";
        } else {
            newClass = "glyphicon-sort-by-attributes-alt";
        }
        arrowSpan.parent().parent().find(".glyphicon").removeClass("glyphicon-resize-vertical").removeClass("glyphicon-sort-by-attributes").removeClass("glyphicon-sort-by-attributes-alt").addClass("glyphicon-resize-vertical");
        arrowSpan.removeClass("glyphicon-resize-vertical").addClass(newClass);
    });

    //Перестроение графика
    $(".rebuildChart").on('click', function (){
            window.chart.clear();
            $(".diagramm_table > tbody > tr > td").css("background-color", "#fff");

            startDate = moment($("#startDate").val(), "DD.MM.YYYY").format('YYYY-MM-DD');
            endDate = moment($("#endDate").val(), "DD.MM.YYYY").format('YYYY-MM-DD');
            setTimePeriod(startDate, endDate);

            //вычитать и записать отмеченные чекбоксы
            alasql("DELETE FROM checkedClients");
            $(".diagramm_table input[type='checkbox']:checked").each(function(i, elem){
                val = $(elem).val();
                if (val != "main"){
                    alasql("INSERT INTO checkedClients value ?", [{"client_id":val}]);
                }
            });

            //Обновить таблицу и перестроить график
            $("#tableDiv").html("").promise().done(buildClientsTable());

            //Перестраиваем график
            $("#chartDiv").html("").promise().done(buildChart());

    });

}

function createContainers(callback){

    tableContainer = "<div id='tableDiv' class='col-lg-3 col-md-3 col-sm-3' style='height: 100%;'></div>";
    chartContainer = "" +
        "<div class='col-lg-9 col-md-9 col-sm-9' style='height:99%; overflow-y: scroll;'>" +
            "<div id='chartDiv' style='width:100%; height: 90%;'></div>" +
            "<div id='legendDiv' style='width:100%; margin-top:10px;'></div>" +
        "</div>";
    $(".diagramm_container").html(tableContainer + chartContainer).promise().done(function () {
        callback();
    });
}

function buildChart() {

    tmpData = alasql("select timestamp_id, timestamp, c.id as client_id, name as client, used_disk_space from tmpData t INNER JOIN client c ON t.client_id = c.id " +
        "WHERE timestamp >= ? AND timestamp <= ? ", [getStartDate(), getEndDate()]);
    timestamps = alasql("select timestamp_id, timestamp from ? group by timestamp_id, timestamp order by timestamp", [tmpData]);
    beginDate = new Date(timestamps[0].timestamp);
    endDate = new Date(timestamps[timestamps.length-1].timestamp);


    graphData = [];
    timestamps.forEach(function (timestampNote, index, array) {
        timestampDataNotes = alasql("SELECT client_id, used_disk_space FROM tmpData WHERE timestamp_id = ?", [timestampNote.timestamp_id]);
        dataNote = {
            'date':timestampNote.timestamp
        };
        timestampDataNotes.forEach(function (item, i, array) {
            if (clientIsChecked(item.client_id)) {
                dataNote[item.client_id] = item.used_disk_space;
            }
        });
        graphData[graphData.length] = dataNote;
    });


    graphs = [];
    clients = alasql("SELECT client_id, name FROM tmpData t INNER JOIN client c ON t.client_id = c.id GROUP BY client_id, name ORDER BY used_disk_space");
    clients.forEach(function (client, i, array) {
        if (clientIsChecked(client.client_id)) {
            note = {
                "bullet":"round",
                "id":"client_" + client.client_id,
                "title":client.name,
                "valueField":client.client_id,
                "balloonText": "[[title]]: [[value]] Мб"
            };
            graphs[graphs.length] = note;
        }
    });

    chartConfig = {
        "type": "serial",
        "categoryField": "date",
        "dataDateFormat": "YYYY-MM-DD",
        "theme": "default",
        "categoryAxis": {
            "parseDates": true
        },
        "chartCursor": {
            "enabled": true,
            "oneBalloonOnly": true,
            "zoomable": false
        },
        "chartScrollbar": {
            "enabled": true
        },
        "trendLines": [],
        "graphs":  graphs,
        "guides": [],
        "valueAxes": [
            {
                "id": "ValueAxis-1",
                "title": "Занято, МБ"
            }
        ],
        "allLabels": [],
        "balloon": {
            "animationDuration": 0,
            "fadeOutDuration": 0
        },
        "legend": {
            "enabled": true,
            "divId":"legendDiv",
            "verticalGap": 5,
            "bottom": 0,
            "equalWidths": true,
            "gradientRotation": 0,
            "left": 0,
            "autoMargins": false,
            "marginLeft": 0,
            "marginRight": 0,
            "maxColumns": 7,
            "position": "bottom",
            "spacing": 0,
            "switchType": "v",
            "textClickEnabled": true,
            "valueText": ": [[value]] Мб",
            "valueWidth": 50,
            "rollOverGraphAlpha": 0
        },
        "dataProvider": graphData
    };



    window.chart = AmCharts.makeChart("chartDiv", chartConfig);
    window.chart.validateData();
    window.chart.graphs.forEach(function (graph, i, array) {
        $(".table_row_client_" + removePrefix(graph.id, "client_") + " > td:nth-child(1)").css("background-color", graph.lineColorR);
    })

}

function setTimePeriod(startDate, endDate){
    alasql("DROP TABLE IF EXISTS timePeriod");
    alasql("CREATE TABLE timePeriod");
    alasql("INSERT INTO timePeriod VALUES ?", [{"startDate":startDate, "endDate":endDate}]);
}

function getStartDate() {
    return alasql("SELECT * FROM timePeriod")[0].startDate;
}

function getEndDate() {
    return alasql("SELECT * FROM timePeriod")[0].endDate;
}

function clientIsChecked(clientId) {
    return alasql("SELECT client_id FROM checkedClients WHERE client_id = ?", [String(clientId)])[0];
}

function createClientsTable(){

    tmpData = alasql("SELECT " +
        "timestamp_id, " +
        "timestamp, " +
        "c.id AS client_id, " +
        "name AS client, " +
        "used_disk_space " +
        "FROM " +
        "tmpData t " +
        "INNER JOIN " +
        "client c " +
        "ON " +
        "t.client_id = c.id " +
        "WHERE " +
        "timestamp >= ? " +
        "AND " +
        "timestamp <= ? ", [getStartDate(), getEndDate()]);

    clients = alasql("SELECT client_id, client FROM ? GROUP BY client_id, client", [tmpData]);

    clients.forEach(function (client, i, array) {
        clients[i].lastUsedSpace = parseInt(alasql("SELECT timestamp_id, used_disk_space FROM ? WHERE client_id = ? ORDER BY timestamp_id DESC LIMIT 1", [tmpData, client.client_id])[0].used_disk_space);
        clients[i].firstUsedSpace =  parseInt(alasql("SELECT timestamp_id, used_disk_space FROM ? WHERE client_id = ? ORDER BY timestamp_id ASC LIMIT 1", [tmpData, client.client_id])[0].used_disk_space);
    });

    clients = alasql("SELECT * FROM ? ORDER BY lastUsedSpace - firstUsedSpace DESC", [clients]);

    infoTable = "" +
        '<table class="diagramm_table table table-fixed">' +
        '<thead>' +
        '<tr class="settings_window">' +
        '<th class="col-xs-5 col-sm-5 col-md-5 col-lg-5 form-group" style="height:auto; margin-bottom: 0;">' +
        '<div class="input-group date" id="datetimepicker_startDate">' +
        '<span class="input-group-addon">' +
        '<span class="glyphicon-calendar glyphicon"></span>' +
        '</span>' +
        '<input type="text" class="form-control" placeholder="Начало" id="startDate">' +
        '</div>' +
        '</th>' +
        '<th class="col-xs-5 col-sm-5 col-md-5 col-lg-5 form-group" style="height:auto; margin-bottom: 0;">' +
        '<div class="input-group date" id="datetimepicker_endDate">' +
        '<span class="input-group-addon">' +
        '<span class="glyphicon-calendar glyphicon"></span>' +
        '</span>' +
        '<input type="text" class="form-control" placeholder="Конец" id="endDate">' +
        '</div>' +
        '</th>' +
        '<th class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-group" style="height:auto; margin-bottom: 0;">' +
        '<button class="btn btn-primary rebuildChart">Build</button>' +
        '</th>' +
        '</tr>' +
        '<tr style="width:97%">' +
        '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" >' +
        '<input type="checkbox" value="main" checked>' +
        '</th>' +
        '<th class="col-xs-5 col-sm-5 col-md-5 col-lg-5 clientNameSort">' +
        'Клиент<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span>' +
        '</th>' +
        '<th class="col-xs-3 col-sm-3 col-md-3 col-lg-3 totalSpaceSort">' +
        'Всего,Гб<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span>' +
        '</th>' +
        '<th class="col-xs-3 col-sm-3 col-md-3 col-lg-3 increaseSpaceSort">' +
        'Прирост,Гб<span class="glyphicon glyphicon-sort-by-attributes-alt pull-right" aria-hidden="true"></span>' +
        '</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody style="height: 90%;">';

    unknownClient = false;
    clients.forEach(function(client, i, array){
        if (clientIsChecked(client.client_id)) {checked = "checked";} else {checked = "";}
        infoTableNote ='' +
            '<tr class="table_row_client_' + client.client_id + '">' +
            '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><input type="checkbox" value="' + client.client_id + '" ' + checked + '></td>' +
            '<td class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="owerflow-x:hidden; text-overflow:clip; white-space:nowrap;">' + client.client + '</td>' +
            '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' + (client.lastUsedSpace/1024).toFixed(1) + '</td>' +
            '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' + ((client.lastUsedSpace - client.firstUsedSpace)/1024).toFixed(1) + '</td>' +
            '</tr>';

        if (client.client_id == "1"){
            unknownClient = infoTableNote;
        } else {
            infoTable += infoTableNote;
        }

    });

    if (unknownClient){
        infoTable += unknownClient;
    }

    infoTable += '' +
        '</tbody>' +
        '</table>';

    return infoTable;
}

function buildClientsTable() {
    tmpData = alasql("SELECT " +
        "timestamp_id, " +
        "timestamp, " +
        "client_id, " +
        "used_disk_space " +
        "FROM " +
        "tmpData t " +
        "WHERE " +
        "timestamp >= ? " +
        "AND " +
        "timestamp <= ? ", [getStartDate(), getEndDate()]);

    clients = alasql("SELECT client_id FROM ? GROUP BY client_id, client", [tmpData]);
    clients.forEach(function (client, i, array) {
        clients[i].lastUsedSpace = parseInt(alasql("SELECT timestamp_id, used_disk_space FROM ? WHERE client_id = ? ORDER BY timestamp_id DESC LIMIT 1", [tmpData, client.client_id])[0].used_disk_space);
        clients[i].firstUsedSpace =  parseInt(alasql("SELECT timestamp_id, used_disk_space FROM ? WHERE client_id = ? ORDER BY timestamp_id ASC LIMIT 1", [tmpData, client.client_id])[0].used_disk_space);
    });

    clientsTable = createClientsTable();

    $("#tableDiv").html(clientsTable).promise().done(function () {

        minDate = moment(alasql("SELECT timestamp FROM tmpData ORDER BY timestamp asc")[0].timestamp, "YYYY-MM-DD HH-mm-ss").format("DD.MM.YYYY");
        maxDate = moment(alasql("SELECT timestamp FROM tmpData ORDER BY timestamp desc")[0].timestamp, "YYYY-MM-DD HH-mm-ss").format("DD.MM.YYYY");

        minYear = moment(minDate, "DD.MM.YYYY").format("YYYY");
        maxYear = moment(maxDate, "DD.MM.YYYY").format("YYYY");


        $(function () {
            $('#datetimepicker_startDate').datetimepicker(
                {
                    pickTime: false,
                    language: 'ru',
                    yearRange: minYear + ":" + maxYear,
                    minDate: minDate,
                    maxDate: maxDate
                }
            );
        });
        $(function () {
            $('#datetimepicker_endDate').datetimepicker(
                {
                    pickTime: false,
                    language: 'ru',
                    yearRange: minYear + ":" + maxYear,
                    minDate: minDate,
                    maxDate: maxDate
                }
            );
        });
        $("#startDate").val(moment(getStartDate()).format('DD.MM.YYYY'));
        $("#endDate").val(moment(getEndDate()).format('DD.MM.YYYY'));
        callTableEventListener();
    });
}

function buildBeginState() {

    createContainers(function(){

        startDate = alasql("SELECT timestamp FROM tmpData ORDER BY timestamp LIMIT 1")[0].timestamp;
        endDate = alasql("SELECT timestamp FROM tmpData ORDER BY timestamp DESC LIMIT 1")[0].timestamp;
        setTimePeriod(moment(startDate).format('YYYY-MM-DD'), moment(endDate).format('YYYY-MM-DD'));

        tmpData = alasql("SELECT " +
            "timestamp_id, " +
            "timestamp, " +
            "client_id, " +
            "used_disk_space " +
            "FROM " +
            "tmpData t " +
            "WHERE " +
            "timestamp >= ? " +
            "AND " +
            "timestamp <= ? ", [getStartDate(), getEndDate()]);

        clients = alasql("SELECT client_id FROM ? GROUP BY client_id, client", [tmpData]);
        clients.forEach(function (client, i, array) {
            clients[i].lastUsedSpace = parseInt(alasql("SELECT timestamp_id, used_disk_space FROM ? WHERE client_id = ? ORDER BY timestamp_id DESC LIMIT 1", [tmpData, client.client_id])[0].used_disk_space);
            clients[i].firstUsedSpace =  parseInt(alasql("SELECT timestamp_id, used_disk_space FROM ? WHERE client_id = ? ORDER BY timestamp_id ASC LIMIT 1", [tmpData, client.client_id])[0].used_disk_space);
        });
        clients = alasql("SELECT client_id FROM ? WHERE client_id != 1 ORDER BY lastUsedSpace - firstUsedSpace DESC LIMIT 10", [clients]);
        alasql("SELECT * INTO checkedClients FROM ?", [clients]);

        buildClientsTable();

    });

    buildChart();

    hideLoader();
}

////////////////////////////////////////////

createDb();
loadStorageList();
loadConfig();
loadTimestampList();
loadClientList();
loadVmList();

$(document).ready(function () {
    window.clTableOrder = [];
    fillStorageList();
    alasql("DROP TABLE IF EXISTS tmpData");
    alasql("CREATE TABLE tmpData");
    alasql("DROP TABLE IF EXISTS checkedClients");
    alasql("CREATE TABLE checkedClients");
    cleanContainer();
    storageId = $("#storageList").val();
    setTimeout(function () {
        getStorageHistoryById(storageId, function (data) {
            buildBeginState(data);
        })
    }, 100);


    //Построение диаграмм и таблицы
    $("#loadStorageInfo").on("click", function () {
        alasql("DROP TABLE IF EXISTS tmpData");
        alasql("CREATE TABLE tmpData");
        alasql("DROP TABLE IF EXISTS checkedClients");
        alasql("CREATE TABLE checkedClients");
        showLoader();
        cleanContainer();
        storageId = $("#storageList").val();
        setTimeout(function () {
            getStorageHistoryById(storageId, function (data) {
                buildBeginState(data);
            })
        }, 100)
    });




});