/**
 * Created by i.kotelnikov on 03.03.2017.
 */

//Показать окно загрузки
function showLoader(){
    setTimeout($(".loader").show(), 0);
}

//Скрыть окно загрузки
function hideLoader() {
    $(".loader").hide();
}

//Подгружает и заполняет список таймстампов
function fillTimestampList() {
    timestamps = alasql("SELECT * FROM timestamp");
    timestampsList = $("#timestampsList");
    timestamps.forEach(function (item, i, array) {
        if (array.length == i+1){  selected = "selected"; } else { selected = ""; }
        if (item['name'] != "") { name = " (" + item['name'] + ")"; } else { name = ""; }
        timestampsList.append("<option " + selected + " value='" + item['id'] + "'>" + item['timestamp'] + name + " </option>");
    });
}

function fillStorageList(){
    storages = alasql ("SELECT * FROM storage WHERE display = '1'");
    storageList = $("#storageList");
    storageList.append("<option selected value='-1'>All DataStores</option>");
    storages.forEach(function (item, i, array) {
        //if (i == 0){  selected = "selected"; } else { selected = ""; }
        selected = "";
        storageList.append("<option " + selected + " value='" + item['id'] + "'>" + item['name'] + name + " </option>");
    });
}

//Очищает контейнер под контент
function cleanContainer() {
    $(".diagramm_container").html('');
}

//Удаляет префикс
function removePrefix(elem, prefix) {
    return elem.replace(prefix, '');
}


//*******************************************************************//
//Создание и инициализация Базы Данных

function createDb(){
    alasql("" +
        "create table config (" +
            "id int, " +
            "parameter string, " +
            "`value` string" +
        ");" +

        "create table timestamp (" +
            "id int, " +
            "name string, " +
            "timestamp string, " +
            "loaded boolean" +
        ");" +

        "create table client (" +
            "id int, " +
            "name string" +
        ");" +

        "create table host (" +
            "id int, " +
            "host string, " +
            "cores int, " +
            "memory int, " +
            "display boolean" +
        ");" +

        "create table host_history (" +
            "id int, " +
            "timestamp_id int, " +
            "host_id int, " +
            "cpu_usage int, " +
            "vcpus int, " +
            "memory_usage int " +
        ");" +

        "create table storage (" +
            "id int, " +
            "name string, " +
            "alias string, " +
            "display boolean" +
        ");" +

        "create table storage_history (" +
            "id int, " +
            "timestamp_id int, " +
            "storage_id int, " +
            "disk_space int" +
        ");" +

        "create table vm (" +
            "id int, " +
            "client_id int, " +
            "name string, " +
            "dns_name string, " +
            "vm_id string" +
        ");" +

        "create table vm_history (" +
            "id int, " +
            "vm_id int, " +
            "timestamp_id int, " +
            "storage_id int, " +
            "host_id int, " +
            "power_state boolean, " +
            "cpu int, " +
            "ram int, " +
            "privisioned_disk_space int, " +
            "used_disk_space int" +
        ");");
}

function initDb(){
    loadConfig();
    loadTimestampList();
    loadClientList();
    loadHostList();
    loadStorageList();
    loadVmList();
}

function loadConfig() {
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getSettings'
        },
        success: function(data){
            settings_list = JSON.parse(data);
            settings_list.forEach(function (item) {
                alasql("INSERT INTO config (id, parameter,`value`) VALUES (?, ?, ?)", [item.id, item.parameter, item.value]);
            });
        }
    });
}

function loadTimestampList() {
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getTimestampList'
        },
        success: function(data){
            timestamp_list = JSON.parse(data);
            timestamp_list.forEach(function (item) {
                if (item.display == 1){
                    alasql("INSERT INTO timestamp (id, name, timestamp, loaded) VALUES (?, ?, ?, ?)", [item.id, item.name, item.timestamp, 0]);
                }
            });
        }
    });


}

function loadHostList() {
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getHostList'
        },
        success: function(data){
            host_list = JSON.parse(data);
            host_list.forEach(function (item) {
                alasql("INSERT INTO host (id, host, cores, memory, display) VALUES (?, ?, ?, ?, ?)",
                    [item.id, item.host, item.cores, item.memory, item.display]);
            });
        },
        async: false
        });
}

function loadStorageList() {
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getStorageList'
        },
        success: function(data){
            storage_list = JSON.parse(data);
            storage_list.forEach(function (item) {
                alasql("INSERT INTO storage (id, name, alias, display) VALUES (?, ?, ?, ?)",
                    [item.id, item.name, item.alias, item.display]);
            });
        },
        async: false
    });
}

function loadClientList() {
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getClientList'
        },
        success:function(data){
            client_list = JSON.parse(data);
            client_list.forEach(function (item) {
                if (item.name == '') { item.name = 'Незвестно'; }
                alasql("INSERT INTO client (id, name) VALUES (?, ?)", [item.id, item.name]);
            });
        }
    });
}

function loadVmList() {
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getVmList'
        },
        success:function(data){
            vm_list = JSON.parse(data);
            vm_list.forEach(function (item) {
                alasql("INSERT INTO vm (id, client_id, name, dns_name, vm_id) VALUES (?, ?, ?, ?, ?, ?)",
                    [item.id, item.client_id, item.name, item.dns_name, item.vm_id]);
            });
        }
    });
}

//********************************************************************//

//Если история не загружена - загружает и отстраивает графики
function loadHistoryByTimestampId(timestampId){

    if (!checkTimestampLoaded(timestampId)){
        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'getHistoryByTimestampId',
                'timestampId': timestampId
            }
        }).done(function(data){
            // console.log(data);
            hist = JSON.parse(data);

            hist['host_history'].forEach(function (item) {
                alasql('INSERT INTO host_history (id, timestamp_id, host_id, cpu_usage, vcpus, memory_usage) VALUES (?, ?, ?, ?, ?, ?)',
                    [item.id, item.timestamp_id, item.host_id, item.cpu_usage, item.vcpus, item.memory_usage]);
            });

            hist['storage_history'].forEach(function (item) {
                alasql('INSERT INTO storage_history (id, timestamp_id, storage_id, disk_space) VALUES (?, ?, ?, ?)',
                    [item.id, item.timestamp_id, item.storage_id, item.disk_space]);
            });

            hist['vm_history'].forEach(function (item) {
                alasql('INSERT INTO vm_history (id, vm_id, timestamp_id, storage_id, host_id, power_state, cpu, ram, privisioned_disk_space, used_disk_space) ' +
                    'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                    [item.id, item.vm_id, item.timestamp_id, item.storage_id, item.host_id, item.power_state, item.cpu, item.ram, item.privisioned_disk_space, item.used_disk_space]);
            });

            alasql("update timestamp SET loaded = 1 WHERE id=?", timestampId);
            buildDiagramms(timestampId);

        });
    } else {
        buildDiagramms(timestampId);
    }
}

//Проверяет, загружена ли история по данному таймстампу
function checkTimestampLoaded(timestamp_id) {
    is_loaded = alasql("SELECT loaded FROM timestamp WHERE id = ?", parseInt(timestamp_id))[0].loaded;
    if (is_loaded){
        return true;
    } else {
        return false;
    }
}

//Отстраивает графики по таймстампу
function buildDiagramms(timestampId) {
    selectedTab = window.diagramms.checkedTab;
    if (selectedTab == 'storage'){

        storageList = alasql("SELECT storage_id FROM vm_history WHERE timestamp_id = ? GROUP BY storage_id ORDER BY storage_id", [timestampId]);


        /////////////////////////////////////
        top_table = '<div>' +
            '<table id="top_table_storage" class="table table-fixed">' +
            '<thead>' +
                '<tr>' +
                    '<th class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right: 20px;">СХД<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span></th>' +
                    '<th class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right: 20px;">Использовано, GB<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span></th>' +
                    '<th class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right: 20px;">Не используется, GB<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span></th>' +
                    '<th class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-right: 20px;">Всего, GB<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span></th>' +
                '</tr>' +
            '</thead>' +
            '<tbody>';


        total_used = 0;
        total_free = 0;
        total_total = 0;
        ////////////////////////////////////////////

        storageList.forEach(function (item, i) {

            storageId = item.storage_id;
            display = alasql("SELECT display FROM storage WHERE id = ?", [storageId])[0].display;
            if (display == 1) {


                storageName = alasql("SELECT name FROM storage WHERE id = ?", storageId)[0].name;
                data = alasql("SELECT used_space/1024  AS used_space, name, 1 AS checked FROM (SELECT storage_id, sum(used_disk_space) AS used_space, client_id FROM vm_history vh inner join vm v on vh.vm_id = v.id WHERE timestamp_id = ? GROUP BY storage_id, client_id ORDER BY storage_id, used_space DESC) rr inner join client cl on cl.id = rr.client_id WHERE storage_id = ?", [timestampId, storageId]);
                used_space = alasql("SELECT sum(used_disk_space)/1024 AS used_space FROM vm_history v WHERE timestamp_id = ? AND storage_id = ?", [timestampId, storageId])[0].used_space;
                total_space = alasql("SELECT disk_space/1024 AS disk_space FROM storage_history s WHERE timestamp_id = ? AND storage_id = ?", [timestampId, storageId])[0].disk_space;
                free_space = total_space - used_space;
                note = {"used_space": free_space, "name": "Not Used", "checked": 1, "color": "#00ee00"};
                data.unshift(note);

                if (data.length < 11) {
                    display_count = data.length;
                    group_size = data[data.length - 1].used_space;
                } else {
                    display_count = 10;
                    group_size = data[10].used_space;
                }
                group_percent = (group_size / total_space) * 100;

                ////////////////////////////////////////////
                top_table += '' +
                    '<tr>' +
                    '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><a href="#container_storage_' + storageId + '">' + storageName + '</a></td>' +
                    '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' + Math.round(used_space).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' + Math.round(free_space).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' + Math.round(total_space).toLocaleString('ru') + '</td>' +
                    '</tr>';

                total_used += Math.round(used_space);
                total_free += Math.round(free_space);
                total_total += Math.round(total_space);
                ////////////////////////////////////////////

                //Формируем таблицу
                data.forEach(function (item, i, array) {
                    if (i == 0) {
                        table_list = '' +
                            '<table id="table_storage_' + storageId + '" class="diagramm_table table table-fixed" style="">' +
                            '<thead>' +
                            '<tr id="settings_window_storage_' + storageId + '" class="settings_window">' +
                            '<th class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group" style="height:auto; margin-bottom: 0;">' +
                            '<div style="display: inline-block;float:left;margin-right: 3px;font-size: 14px;">Показывать:</div>' +
                            '<input type="range" min="1" max="' + array.length + '" class="range_settings_window form-control" value="10" style="">' +
                            '<input type="number" min="1" max="' + array.length + '" pattern="\d*" class="number_settings_window form-control" value="10" style="">' +
                            '<div style="font-size: 14px; display: inline-block; float: left;">из <span class="total_disk_space">' + array.length + '</span></div>' +
                            '</th>' +
                            '</tr>' +
                            '<tr>' +
                            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" >' +
                            '<input type="checkbox" value="main" checked>' +
                            '</th>' +
                            '<th class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="cursor: pointer">' +
                            'Клиент' +
                            '<span class="glyphicon glyphicon-resize-vertical pull-right" aria-hidden="true"></span>' +
                            '</th>' +
                            '<th class="col-xs-4 col-sm-4 col-md-4 col-lg-4">' +
                            'Использовано, GB' +
                            '<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span>' +
                            '</th>' +
                            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' +
                            '<span id="settings_storage_' + storageId + '" class="glyphicon glyphicon-cog settings" aria-hidden="true" style="display: none;"></span>' +
                            '</th>' +
                            '</tr>' +

                            '</thead>' +
                            '<tbody style="height: 90%;">';

                    }
                    item.used_space = Math.round(item.used_space * 100) / 100;
                    table_list += '' +
                        '<tr>' +
                        '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><input type="checkbox" value="' + item.name + '" checked></td>' +
                        '<td class="col-xs-6 col-sm-6 col-md-6 col-lg-6">' + item.name + '</td>' +

                        '<td class="col-xs-4 col-sm-4 col-md-4 col-lg-4">' + item.used_space + '</td>' +
                        '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><!--<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>--></td>' +
                        '</tr>';

                    if (i == array.length - 1) {
                        table_list += '</tbody></table>';
                    }

                });

                $(".diagramm_container").append(
                    '<div id="container_storage_' + storageId + '" style="width: 100%; height: 95%; border-left:1px solid #ccc; border-right:1px solid #ccc; border-top:1px solid #ccc;display: inline-block; ">' +
                    '<div id="headerdiv_storage_' + storageId + '" style="height:5.5%; width:100%; border-bottom:1px solid #ccc;">' +
                    '<div style="display: inline-block; padding-left:10px;"><h5><a href="#top_table_storage" title=Наверх>Наверх</a></h5></div>' +
                    '<div style="display: inline-block; text-align:center; width:90%;">' +
                    '<h5>' +
                    '<span class="" id="storage_header_info_' + storageId + '">' +
                    '<strong>' + storageName + '</strong> (Всего: <strong>' + Math.round(total_space).toLocaleString('ru') + ' GB</strong> | Использовано: <strong style="color:red;">' + Math.round(used_space).toLocaleString('ru') + ' GB</strong> | Свободно: <strong style="color:green;">' + Math.round(free_space).toLocaleString('ru') + ' GB</strong>)' +
                    '</span>' +
                    '</h5>' +
                    '</div>' +
                    '</div>' +
                    '<div id="table_storage_' + storageId + '" style="border-right:1px solid #ccc; height:94%; width:33%; display: inline-block; vertical-align: top; overflow:hidden;">' +
                    table_list +
                    '</div>' +
                    '<div id="piechart_storage_' + storageId + '" style="height:94.5%; width:67%; display: inline-block; "></div>' +
                    '</div>');

                //Создаем объект графика и заливаем его
                chart = new AmCharts.AmPieChart();
                chart.numberFormatter = {precision: 2, decimalSeparator: '.', thousANDsSeparator: ' '};
                chart.colorField = "color";
                chart.groupedColor = "#8b8989";
                chart.dataProvider = data;
                chart.titleField = "name";
                chart.valueField = "used_space";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.1;
                chart.outlineThickness = 1;
                chart.groupPercent = group_percent;
                chart.depth3D = 5;
                chart.labelRadius = 60;
                chart.startEffect = "easeOutSine";
                chart.startRadius = "50";
                chart.labelText = '[[title]]: [[value]] GB';
                chart.thousANDsSeparator = " ";
                chart.write("piechart_storage_" + storageId);

                //Записываем диаграмму в глобальную область видимости (график, дата, количество не группируемых клиентов.
                window.diagramms.storage[storageId] = {
                    'chart': chart,
                    'chartData': data,
                    'displayCount': display_count
                };
            }
        });

        //////////////////////////////////////////////////////////////////
        top_table += '' +
            '<tr>' +
            '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><strong>Всего</strong></td>' +
            '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><strong>' + Math.round(total_used).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><strong>' + Math.round(total_free).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><strong>' + Math.round(total_total).toLocaleString('ru') + '</strong></td>' +
            '</tr>';

        top_table += '</tbody></table></div>';
        $(".diagramm_container").prepend(top_table);
        //////////////////////////////////////////////////////////////////


        hideLoader();

    } else
    if (selectedTab == 'host'){

        hostList = alasql("SELECT host_id FROM vm_history WHERE timestamp_id = ? GROUP BY host_id ORDER BY host_id", [timestampId]);

        /////////////////////////////////////
        top_table = '<div>' +
            '<table id="top_table_host" class="table table-fixed">' +
            '<thead>' +
            '<tr>' +
            '<th class="col-xs-4 col-sm-4 col-md-4 col-lg-4" rowspan="2" style="padding-right: 20px; height:75px; padding-top: 30px; border-right: 1px solid #ccc;">Хост</th>' +
            '<th class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: center">CPU</th>' +
            '<th class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: center">RAM</th>' +
            '</tr>' +
            '<tr>' +
            // '<th class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align: center; height: 0;"></th>' +
            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left">Занято</th>' +
            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left">Свободно</th>' +
            '<th class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="text-align: left">Расчет макс</th>' +
            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left">Занято</th>' +
            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left">Свободно</span></th>' +
            '<th class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="text-align: left">Расчет макс</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';


        total_used_cpu = 0;
        total_free_cpu = 0;
        total_total_cpu = 0;

        total_used_ram = 0;
        total_free_ram = 0;
        total_total_ram = 0;
        ////////////////////////////////////////////

        hostList.forEach(function (item, i) {

            hostId = item.host_id;

            display = alasql("SELECT display FROM host WHERE id = ?", [hostId])[0].display;
            if (display == 1) {

                hostName = alasql("SELECT host FROM host WHERE id = ?", hostId)[0].host;

                data = alasql("" +
                    "SELECT " +
                    "cpu, ram, name, 1 AS checked " +
                    "FROM " +
                    "(" +
                    "SELECT " +
                    "host_id, sum(ram)/1024 AS ram, sum(cpu) AS cpu, client_id " +
                    "FROM " +
                    "vm_history vh " +
                    "INNER JOIN " +
                    "vm v " +
                    "ON " +
                    "vh.vm_id = v.id " +
                    "WHERE " +
                    "timestamp_id = ? " +
                    "GROUP BY " +
                    "host_id, client_id" +
                    ") rr " +
                    "INNER JOIN " +
                    "client cl " +
                    "ON " +
                    "cl.id = rr.client_id " +
                    "WHERE " +
                    "host_id = ? " +
                    "ORDER BY " +
                    "host_id ASC, " +
                    "cpu DESC, " +
                    "ram DESC;",
                    [timestampId, hostId]
                );

                used_cpu = alasql("" +
                    "SELECT " +
                    "sum(cpu) AS cpu " +
                    "FROM " +
                    "vm_history v " +
                    "WHERE " +
                    "timestamp_id = ? " +
                    "AND " +
                    "host_id = ?",
                    [timestampId, hostId]
                )[0].cpu;

                used_ram = alasql("" +
                    "SELECT " +
                    "sum(ram)/1024 AS ram " +
                    "FROM " +
                    "vm_history v " +
                    "WHERE " +
                    "timestamp_id = ? " +
                    "AND " +
                    "host_id = ?",
                    [timestampId, hostId]
                )[0].ram;

                //Вынести в настройку на сервере
                max_cpu_usage_percent = parseInt(alasql("SELECT `value` FROM config WHERE parameter = 'max_cpu_usage_percent'")[0].value);
                current_cpu_usage_percent = alasql("SELECT cpu_usage FROM host_history WHERE timestamp_id = ? AND host_id = ?", [timestampId, hostId])[0].cpu_usage;
                current_cpu_count = alasql("SELECT vcpus FROM host_history WHERE timestamp_id = ? AND host_id = ?", [timestampId, hostId])[0].vcpus;
                //Будем округлять в большую сторону, потому что почему бы и нет.
                total_cpu_count = Math.ceil((max_cpu_usage_percent * current_cpu_count) / current_cpu_usage_percent);

                //Вынести в настройку на сервере
                max_ram_usage_percent = parseInt(alasql("SELECT `value` FROM config WHERE parameter = 'max_ram_usage_percent'")[0].value);
                current_ram_usage_percent = alasql("SELECT memory_usage FROM host_history WHERE timestamp_id = ? AND host_id = ?", [timestampId, hostId])[0].memory_usage;
                current_ram_count = alasql("SELECT sum(ram) AS ram FROM vm_history v WHERE timestamp_id = ? AND host_id = ?", [timestampId, hostId])[0].ram / 1024;
                total_ram_count = Math.ceil((max_ram_usage_percent * current_ram_count) / current_ram_usage_percent);

                free_cpu = total_cpu_count - current_cpu_count;

                free_ram = total_ram_count - current_ram_count;

                note = {"cpu": free_cpu, "ram": free_ram, "name": "Not Used", "checked": 1, "color": "#00ee00"};

                data.unshift(note);

                if (data.length < 11) {
                    display_count = data.length;
                    group_size_cpu = data[data.length - 1].cpu;
                } else {
                    display_count = 10;
                    group_size_cpu = data[10].cpu;
                }

                group_percent_cpu = (group_size_cpu / total_cpu_count) * 100;


                if (data.length < 11) {
                    display_count = data.length;
                    group_size_ram = data[data.length - 1].ram;
                } else {
                    display_count = 10;
                    group_size_ram = data[10].ram;
                }

                group_percent_ram = (group_size_ram / total_ram_count) * 100;

                ////////////////////////////////////////////
                top_table += '' +
                    '<tr>' +
                    '<td class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><a href="#container_host_' + hostId + '">' + hostName + '</a></td>' +
                    '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' + Math.round(used_cpu).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' + Math.round(free_cpu).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' + Math.round(total_cpu_count).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' + Math.round(used_ram).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' + Math.round(free_ram).toLocaleString('ru') + '</td>' +
                    '<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' + Math.round(total_ram_count).toLocaleString('ru') + '</td>' +
                    '</tr>';

                total_used_cpu += Math.round(used_cpu);
                total_free_cpu += Math.round(free_cpu);
                total_total_cpu += Math.round(total_cpu_count);

                total_used_ram += Math.round(used_ram);
                total_free_ram += Math.round(free_ram);
                total_total_ram += Math.round(total_ram_count);

                ////////////////////////////////////////////


                data.forEach(function (item, i, array) {

                    if (i == 0) {
                        table_list = '' +
                            '<table id="table_host_' + hostId + '" class="diagramm_table table table-fixed">' +
                            '<thead>' +
                            '<tr id="settings_window_host_' + hostId + '" class="settings_window">' +
                            '<th class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group" style="height:auto; margin-bottom: 0;">' +
                            '<div style="display: inline-block;float:left;margin-right: 3px;font-size: 14px;">Показывать:</div>' +
                            '<input type="range" min="1" max="' + array.length + '" class="range_settings_window form-control" value="10" style="">' +
                            '<input type="number" min="1" max="' + array.length + '" pattern="\d*" class="number_settings_window form-control" value="10">' +
                            '<div style="font-size: 14px; display: inline-block; float: left;">из <span class="total_disk_space">' + array.length + '</span></div>' +
                            '</th>' +
                            '</tr>' +

                            '<tr>' +
                            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1" >' +
                            '<input type="checkbox" value="main" checked>' +
                            '</th>' +
                            '<th class="col-xs-6 col-sm-6 col-md-6 col-lg-6">' +
                            'Клиент' +
                            '<span class="glyphicon glyphicon-resize-vertical pull-right" aria-hidden="true"></span>' +
                            '</th>' +
                            '<th class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' +
                            'CPU' +
                            '<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span>' +
                            '</th>' +
                            '<th class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' +
                            'RAM' +
                            '<span class="glyphicon glyphicon-resize-vertical  pull-right" aria-hidden="true"></span>' +
                            '</th>' +
                            '<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1">' +
                            '<span id="settings_host_' + hostId + '" class="glyphicon glyphicon-cog settings" aria-hidden="true" style="display: none;"></span>' +
                            '</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody style="height: 90%;">';
                    }

                    item.ram = Math.round(item.ram);

                    table_list += '' +
                        '<tr>' +
                        '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><input type="checkbox" value="' + item.name + '" checked></td>' +
                        '<td class="col-xs-6 col-sm-6 col-md-6 col-lg-6">' + item.name + '</td>' +
                        '<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' + item.cpu + '</td>' +
                        '<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2">' + item.ram + '</td>' +
                        '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><!--<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>--></td>' +
                        '</tr>';

                    if (i == array.length - 1) {
                        table_list += '</tbody></table>';
                    }
                });

                $(".diagramm_container").append(
                    '<div id="container_host_' + hostId + '" style="width: 100%; height: 95%; border-left:1px solid #ccc; border-right:1px solid #ccc; border-top:1px solid #ccc;display: inline-block; ">' +
                    '<div id="headerdiv_host_' + hostId + '" style="height:5.5%; width:100%; border-bottom:1px solid #ccc;">' +
                    '<div style="display:inline-block; padding-left:10px;"><h5><a href="#top_table_host" title="Наверх">Наверх</a></h5></div>' +
                    '<div id="header_div_ram_host_' + hostId + '" style="display: inline-block; text-align:center; width:80%;">' +
                    '<h5>' +
                    '<span id="host_header_info_ram_' + hostId + '">' +
                    '<strong>' + hostName + '</strong> (Всего: <strong>' + Math.round(total_ram_count).toLocaleString('ru') + ' GB</strong> | Занято: <strong style="color:red;">' + Math.round(used_ram).toLocaleString('ru') + ' GB</strong> | Свободно: <strong style="color:green;">' + Math.round(free_ram).toLocaleString('ru') + ' GB</strong>)' +
                    '</span>' +
                    '</h5>' +
                    '</div>' +

                    '<div id="header_div_cpu_host_' + hostId + '" style="display: inline-block; text-align:center; width:80%;">' +
                    '<h5>' +
                    '<span id="host_header_info_cpu_' + hostId + '">' +
                    '<strong>' + hostName + '</strong> (Всего: <strong>' + Math.round(total_cpu_count).toLocaleString('ru') + ' CPU</strong> | Занято: <strong style="color:red;">' + Math.round(used_cpu).toLocaleString('ru') + ' CPU</strong> | Свободно: <strong style="color:green;">' + Math.round(free_cpu).toLocaleString('ru') + ' CPU</strong>)' +
                    '</span>' +
                    '</h5>' +
                    '</div>' +

                    '<div class="input-group pull-right" style="display: inline; height:100%; position:relative; width:100px; margin-right:10px;">' +
                    '<a id="swich_to_cpu_host_' + hostId + '" class="swich_to_cpu form-control btn btn-default active" style="height:100%; display: inline-block; width: 50%">CPU</a>' +
                    '<a id="swich_to_ram_host_' + hostId + '" class="swich_to_ram form-control btn btn-default " style="height:100%; display: inline-block; width: 50%">RAM</a>' +
                    '</div>' +
                    '</div>' +

                    '<div id="table_host_' + hostId + '" style="border-right:1px solid #ccc; height:94%; width:33%; display: inline-block; vertical-align: top; overflow:hidden;">' +
                    table_list +
                    '</div>' +

                    '<div id="piechart_host_cpu_' + hostId + '" style="height:94.5%; width:67%; display: inline-block; "></div>' +
                    '<div id="piechart_host_ram_' + hostId + '" style="height:94.5%; width:67%; display:inline-block; "></div>' +
                    '</div>');

                $("#piechart_host_ram_" + hostId).hide();
                $("#header_div_ram_host_" + hostId).hide();

                chart_ram = new AmCharts.AmPieChart();
                chart_ram.numberFormatter = {precision: 2, decimalSeparator: '.', thousANDsSeparator: ' '};
                chart_ram.colorField = "color";
                chart_ram.groupedColor = "#8b8989";
                chart_ram.dataProvider = data;
                chart_ram.titleField = "name";
                chart_ram.valueField = "ram";
                chart_ram.outlineColor = "#FFFFFF";
                chart_ram.outlineAlpha = 0.1;
                chart_ram.outlineThickness = 1;
                chart_ram.groupPercent = group_percent_ram;
                chart_ram.depth3D = 5;
                chart_ram.labelRadius = 60;
                chart_ram.startEffect = "easeOutSine";
                chart_ram.startRadius = "50";
                chart_ram.labelText = '[[title]]: [[value]] GB';
                chart_ram.thousANDsSeparator = " ";
                chart_ram.write("piechart_host_ram_" + hostId);


                chart_cpu = new AmCharts.AmPieChart();
                chart_cpu.numberFormatter = {precision: 2, decimalSeparator: '.', thousANDsSeparator: ' '};
                chart_cpu.colorField = "color";
                chart_cpu.groupedColor = "#8b8989";
                chart_cpu.dataProvider = data;
                chart_cpu.titleField = "name";
                chart_cpu.valueField = "cpu";
                chart_cpu.outlineColor = "#FFFFFF";
                chart_cpu.outlineAlpha = 0.1;
                chart_cpu.outlineThickness = 1;
                chart_cpu.groupPercent = group_percent_cpu;
                chart_cpu.depth3D = 5;
                chart_cpu.labelRadius = 60;
                chart_cpu.startEffect = "easeOutSine";
                chart_cpu.startRadius = "50";
                chart_cpu.labelText = '[[title]]: [[value]]';
                chart_cpu.thousANDsSeparator = " ";
                chart_cpu.write("piechart_host_cpu_" + hostId);

                // window.diagramms.storage[storageId] = {
                //     'chart':chart,
                //     'chartData':data,
                //     'displayCount':display_count
                // };


                window.diagramms.host[hostId] = {
                    'chart_ram': chart_ram,
                    'chart_cpu': chart_cpu,
                    'chartData': data,
                    'displayCount': display_count
                };
            }
        });

        //////////////////////////////////////////////////////////////////
        top_table += '' +
            '<tr>' +
            '<td class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><strong>Всего</strong></td>' +
            '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><strong>' + Math.round(total_used_cpu).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><strong>' + Math.round(total_free_cpu).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><strong>' + Math.round(total_total_cpu).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><strong>' + Math.round(total_used_ram).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><strong>' + Math.round(total_free_ram).toLocaleString('ru') + '</strong></td>' +
            '<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><strong>' + Math.round(total_total_ram).toLocaleString('ru') + '</strong></td>' +
            '</tr>';

        top_table += '</tbody></table></div>';
        $(".diagramm_container").prepend(top_table);
        //////////////////////////////////////////////////////////////////

        hideLoader();

    }
    $("td").each(function(index, element){ if (parseFloat($(element).text())<0){$(element).addClass("danger");}});
    callTableEventListener();

}

//Выгрудаем всю историю по ДатаСтору, если хотим историю по всем, то передаем storageId = -1
function getStorageHistoryById(storageId,callback){
    $.ajax({
        type: 'POST',
        url: 'action.php',
        data: {
            'action': 'getStorageHistoryById',
            'storageId': storageId
        },
        success: function(data){
            data = JSON.parse(data);
            alasql('SELECT * INTO tmpData FROM ?',[data]);
            callback();
        }
    });
}





