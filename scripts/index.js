//Создает Event-Listener'ы для таблицы
function callTableEventListener(){

    //Реакция на чек-боксы
    $("input[type='checkbox']").on('click', function () {

        if ($(this).is(':checked')){
            checked = 1;
        } else {
            checked = 0;
        }

        client = $(this).val();
        full_id = $(this).parent().parent().parent().parent().attr('id');

        if (full_id.indexOf('storage') + 1){

            id = removePrefix(full_id, 'table_storage_');

            if (client=='main'){
                $("#"+full_id+" input[type='checkbox']").prop("checked", checked);
                newData = alasql("SELECT used_space, name, ? AS checked FROM ? WHERE name != 'Free'", [checked, window.diagramms.storage[id].chartData]);
                free = alasql("SELECT used_space, name, ? AS checked, '#00ee00' AS color FROM ? WHERE name = 'Free'", [checked, window.diagramms.storage[id].chartData])[0];
                newData.unshift(free);
                window.diagramms.storage[id].chartData = newData;
            } else {



                newData = alasql("SELECT * FROM ? WHERE name != ? ", [window.diagramms.storage[id].chartData, client]);
                chosen = alasql("SELECT * FROM ? WHERE name = ? ", [window.diagramms.storage[id].chartData, client])[0];
                chosen.checked = checked;
                newData.unshift(chosen);
                window.diagramms.storage[id].chartData = newData;
            }

            newData = alasql("SELECT * FROM ? WHERE checked = 1 ORDER BY used_space DESC", [window.diagramms.storage[id].chartData]);

            total_space = alasql("SELECT sum(used_space) AS used_space FROM ? WHERE checked = 1", [window.diagramms.storage[id].chartData])[0].used_space;

            selected_count = newData.length;
            display_count = window.diagramms.storage[id].displayCount;


            if (display_count >= selected_count){
                new_groupPercent = 0;
            } else {
                groupSize = newData[display_count - 1].used_space;
                new_groupPercent = (groupSize/total_space)*100;
            }

            table_parent = $(this).parent().parent().parent().parent();
            table_parent.find(".total_disk_space").text(newData.length);
            num_input = table_parent.find(".number_settings_window");
            range_input = table_parent.find(".range_settings_window");

            max = newData.length;
            if (max == 0){
                num_input.attr("max", newData.length);
                range_input.attr("max", 1);
            } else {
                num_input.attr("max", newData.length);
                range_input.attr("max", newData.length);
            }


            window.diagramms.storage[id].chart.groupPercent = new_groupPercent;
            window.diagramms.storage[id].chart.dataProvider = newData;
            window.diagramms.storage[id].chart.validateData();

        } else
        if (full_id.indexOf('host') + 1){

            id = removePrefix(full_id, 'table_host_');



            if (client=='main'){
                $("#"+full_id+" input[type='checkbox']").prop("checked", checked);
                newData = alasql("SELECT cpu, ram, name, ? AS checked FROM ? WHERE name != 'Free'", [checked, window.diagramms.host[id].chartData]);
                free = alasql("SELECT cpu, ram, name, ? AS checked, '#00ee00' AS color FROM ? WHERE name = 'Free'", [checked, window.diagramms.host[id].chartData])[0];
                newData.unshift(free);
                window.diagramms.host[id].chartData = newData;
            } else {
                newData = alasql("SELECT * FROM ? WHERE name != ? ", [window.diagramms.host[id].chartData, client]);
                chosen = alasql("SELECT * FROM ? WHERE name = ? ", [window.diagramms.host[id].chartData, client])[0];
                chosen.checked = checked;
                newData.unshift(chosen);
                window.diagramms.host[id].chartData = newData;
            }

            newData = alasql("SELECT * FROM ? WHERE checked = 1", [window.diagramms.host[id].chartData]);

            total_cpu_count = alasql("SELECT sum(cpu) AS cpu FROM ? WHERE checked = 1", [window.diagramms.host[id].chartData])[0].cpu;
            total_ram_count = alasql("SELECT sum(ram) AS ram FROM ? WHERE checked = 1", [window.diagramms.host[id].chartData])[0].ram;

            selected_count = newData.length;
            display_count = window.diagramms.host[id].displayCount;


            if (display_count >= selected_count){
                new_groupPercent_cpu = 0;
                new_groupPercent_ram = 0;
            } else {
                groupSize_cpu = newData[display_count - 1].cpu;
                groupSize_ram = newData[display_count - 1].ram;

                new_groupPercent_cpu = (groupSize_cpu/total_cpu_count)*100;
                new_groupPercent_ram = (groupSize_ram/total_ram_count)*100;
            }

            table_parent = $(this).parent().parent().parent().parent();
            table_parent.find(".total_disk_space").text(newData.length);
            num_input = table_parent.find(".number_settings_window");
            range_input = table_parent.find(".range_settings_window");

            max = newData.length;

            if (max == 0){
                num_input.attr("max", newData.length);
                range_input.attr("max", 1);
            } else {
                num_input.attr("max", newData.length);
                range_input.attr("max", newData.length);
            }

            newData_cpu = alasql("SELECT * FROM ? ORDER BY cpu DESC", [newData]);
            newData_ram = alasql("SELECT * FROM ? ORDER BY ram DESC", [newData]);

            window.diagramms.host[id].chart_cpu.dataProvider = newData_cpu;
            window.diagramms.host[id].chart_ram.dataProvider = newData_ram;

            window.diagramms.host[id].chart_cpu.groupPercent = new_groupPercent_cpu;
            window.diagramms.host[id].chart_ram.groupPercent = new_groupPercent_ram;

            window.diagramms.host[id].chart_cpu.validateData();
            window.diagramms.host[id].chart_ram.validateData();

        }

    });

    //Реакция на изменение значений порогов для числового поля
    $(".number_settings_window").on('input', function(){
        if (!Number.isInteger(parseInt($(this).val())) && $(this).val()!=''){
            $(this).val(10);
        } else if (parseInt($(this).val()) > $(this).attr('max')){
            $(this).val($(this).attr('max'));
        } else if (parseInt($(this).val()) < $(this).attr('min')){
            $(this).val($(this).attr('min'));
        }
        display_count = parseInt($(this).val());
        $(this).parent().find(".range_settings_window").val(display_count);

    });

    //Реакция на изменение значений порогов для бегунка
    $(".range_settings_window").on('input', function(){
        display_count = parseInt($(this).val());
        $(this).parent().find(".number_settings_window").val(display_count);
    });

    //Реакция на сортировку
    $('.table').DataTable({
        paging:false,
        searching:false,
        bInfo : false
    });
    //Костыль, чтоб не сбивались стили таблицы после добавления к ней сортировщика
    $("table").removeAttr("style");
    $("thead > :nth-child(2) > th").removeAttr("style");
    $("thead").css('cursor', 'pointer');
    $(".diagramm_table").removeAttr("style");

    $(".table th").on("click", function () {
        arrowSpan = $(this).find(".glyphicon");
        if (arrowSpan.hasClass("glyphicon-resize-vertical") || arrowSpan.hasClass("glyphicon-sort-by-attributes-alt")){
            newClass = "glyphicon-sort-by-attributes";
        } else {
            newClass = "glyphicon-sort-by-attributes-alt";
        }
        arrowSpan.parent().parent().find(".glyphicon").removeClass("glyphicon-resize-vertical").removeClass("glyphicon-sort-by-attributes").removeClass("glyphicon-sort-by-attributes-alt").addClass("glyphicon-resize-vertical");
        arrowSpan.removeClass("glyphicon-resize-vertical").addClass(newClass);
    });

}


//**************** НАЧАЛО ИСПОЛНЯЕМОГО СКРИПТА *********************//

createDb();
initDb();

//****** НАЧАЛО ИСПОЛНЯЕМОГО СКРИПТА ПОСЛЕ ЗАГРУЗКИ СТРАНИЦЫ *******//
$(document).ready(function () {

    $("#settings_window").hide();
    window.diagramms = {};
    window.diagramms.checkedTab = 'storage';
    window.diagramms.storage = {};
    window.diagramms.host = {};

    function initial(){
        fillTimestampList();
        //При загрузке списка выбираем самый последний таймстамп
        window.diagramms.chosenTimestamp = alasql("SELECT id FROM timestamp ORDER BY id DESC LIMIT 1")[0]["id"];
        loadHistoryByTimestampId(window.diagramms.chosenTimestamp);
    }
    setTimeout(initial, 150);

    //Построение диаграмм и таблицы
    $("#buildDiagramms").on("click", function () {
        showLoader();
        cleanContainer();
        timestampId = $("#timestampsList").val();
        window.diagramms.chosenTimestamp = timestampId;
        setTimeout(function () {
            loadHistoryByTimestampId(parseInt(timestampId));
        }, 100)
    });

    //Переключение на вкладку кластера
    $("#hostTab").on("click", function () {
        if (!$(this).hasClass("active")){
            showLoader();
            cleanContainer();
            $("#storageTab").removeClass("active");
            $(this).addClass("active");
            window.diagramms.checkedTab = 'host';
            setTimeout(function () {
                loadHistoryByTimestampId(parseInt(window.diagramms.chosenTimestamp));
                $(".swich_to_cpu").on("click", function () {
                    hostId = removePrefix($(this).attr('id'), 'swich_to_cpu_host_');
                    $("#swich_to_ram_host_" + hostId).removeClass("active");
                    $(this).addClass("active");
                    $("#piechart_host_ram_" + hostId).hide();
                    $("#piechart_host_cpu_" + hostId).show();
                    $("#header_div_ram_host_" + hostId).hide();
                    $("#header_div_cpu_host_" + hostId).show();
                });

                $(".swich_to_ram").on("click", function () {
                    hostId = removePrefix($(this).attr('id'), 'swich_to_ram_host_');
                    $("#swich_to_cpu_host_" + hostId).removeClass("active");
                    $(this).addClass("active");
                    $("#piechart_host_cpu_" + hostId).hide();
                    $("#piechart_host_ram_" + hostId).show();
                    $("#header_div_cpu_host_" + hostId).hide();
                    $("#header_div_ram_host_" + hostId).show();
                });
            }, 100);

        }
    });

    //Переключение на вкладку Стораджа
    $("#storageTab").on("click", function () {
        if (!$(this).hasClass("active")){
            showLoader();
            cleanContainer();
            $("#hostTab").removeClass("active");
            $(this).addClass("active");
            window.diagramms.checkedTab = 'storage';
            setTimeout(function () {
                loadHistoryByTimestampId(parseInt(window.diagramms.chosenTimestamp));
            }, 100);
        }
    });

    //Применяем изменения порогов компановки графика.
    $('html').keydown(function(eventObject){ //отлавливаем нажатие клавиш
        if ((event.keyCode == 13) && (($("input:focus").attr('type') == 'range') || ($("input:focus").attr('type') == 'number'))) { //если нажали Enter, то true
            input = $("input:focus");
            if(input.val()){

                display_count = parseInt(input.val());

                full_id = removePrefix(input.parent().parent().attr("id"), "settings_window_"); //storage_# либо cluster_#
                if (full_id.indexOf('storage') + 1){

                    tab = 'storage';
                    storageId = removePrefix(full_id, tab + '_');
                    checked_notes_count = alasql("SELECT count(*) AS notes_count FROM ? WHERE checked = 1", [window.diagramms.storage[storageId].chartData])[0].notes_count;
                    //Если хотим показать больше записей, чем отмечено показывать - показываем все отмеченные.
                    if (checked_notes_count <= display_count){
                        newGroupPercent = 0;
                    } else {
                        group_space_limit = alasql("SELECT used_space FROM ? WHERE checked = 1 ORDER BY used_space DESC", [window.diagramms.storage[storageId].chartData])[display_count - 1].used_space;
                        total_space = alasql("SELECT sum(used_space) AS total_space FROM ? WHERE checked = 1", [window.diagramms.storage[storageId].chartData])[0].total_space;
                        newGroupPercent = (group_space_limit/total_space)*100;
                    }

                    window.diagramms.storage[storageId].displayCount = display_count;
                    window.diagramms.storage[storageId].chart.groupPercent = newGroupPercent;
                    window.diagramms.storage[storageId].chart.validateData();

                } else if (full_id.indexOf('host') + 1){
                    tab = 'host';

                    hostId = removePrefix(full_id, tab + '_');

                    checked_notes_count = alasql("SELECT count(*) AS notes_count FROM ? WHERE checked = 1", [window.diagramms.host[hostId].chartData])[0].notes_count;
                    //Если хотим показать больше записей, чем отмечено показывать - показываем все отмеченные.
                    if (checked_notes_count <= display_count){
                        newGroupPercent = 0;
                    } else {
                        group_cpu_limit = alasql("SELECT cpu FROM ? WHERE checked = 1 ORDER BY cpu DESC", [window.diagramms.host[hostId].chartData])[display_count - 1].cpu;
                        group_ram_limit = alasql("SELECT ram FROM ? WHERE checked = 1 ORDER BY ram DESC", [window.diagramms.host[hostId].chartData])[display_count - 1].ram;
                        //group_space_limit = alasql("SELECT used_space FROM ? WHERE checked = 1 ORDER BY used_space DESC", [window.diagramms.storage[hostId].chartData])[display_count - 1].used_space;

                        total_cpu = alasql("SELECT sum(cpu) AS total_cpu FROM ? WHERE checked = 1", [window.diagramms.host[hostId].chartData])[0].total_cpu;
                        total_ram = alasql("SELECT sum(ram) AS total_ram FROM ? WHERE checked = 1", [window.diagramms.host[hostId].chartData])[0].total_ram;
                        //total_space = alasql("SELECT sum(used_space) AS total_space FROM ? WHERE checked = 1", [window.diagramms.storage[hostId].chartData])[0].total_space;

                        newGroupPercent_cpu = (group_cpu_limit/total_cpu)*100;
                        newGroupPercent_ram = (group_ram_limit/total_ram)*100;
                        //newGroupPercent = (group_space_limit/total_space)*100;
                    }

                    window.diagramms.host[hostId].displayCount = display_count;

                    window.diagramms.host[hostId].chart_ram.groupPercent = newGroupPercent_ram;
                    window.diagramms.host[hostId].chart_ram.validateData();

                    window.diagramms.host[hostId].chart_cpu.groupPercent = newGroupPercent_cpu;
                    window.diagramms.host[hostId].chart_cpu.validateData();

                }

                // console.log(tab);
                // console.log(checked_notes_count);
                // console.log(storageId);
                // console.log(display_count);
            }
        }
    });

});