<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="shortcut icon" href="../mainSource/favicon.ico" type="image/x-icon" />

    <script src="../mainSource/jquery-2.2.4/jquery.min.js"></script>

    <script src="../mainSource/moment/moment-with-locales.min.js"></script>

    <script src="../mainSource/bootstrap-3.3.7/js/bootstrap.min.js"></script>

    <script src="../mainSource/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <link href="../mainSource/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="../mainSource/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/i18n/defaults-ru_RU.min.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.format.js"></script>

    <script src="../mainSource/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>

    <script language="javascript" type="text/javascript" src="scripts/alasql/console/alasql.min.js"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/amcharts.js" type="text/javascript"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/pie.js" type="text/javascript"></script>

    <!--    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">-->

    <script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" type="text/css" href="style/style.css">

    <title>
        RitmTools
    </title>

</head>
<body style="height: 100%;">
<div class="container" style="height: 100%;">

    <div class="row" style="height: 10%; border-left:1px solid #ccc; border-right:1px solid #ccc;">

        <div class="col-lg-3 col-md-3" style="text-align:center;">
        </div>

        <div class="col-lg-6 col-md-6" style="">
            <h1 style="text-align: center">Настройки</h1>
        </div>

        <div class="col-lg-3 col-md-3" style="text-align:center;">

        </div>

    </div>

    <div class="row" style="height: 90%;  border-left:1px solid #ccc; border-right:1px solid #ccc;">
        <div class="col-lg-3 col-md-3" style="height: 100%; overflow-y: hidden;">
            <ul id="swich_settings_list" class="nav nav-pills nav-stacked">
                <li role="presentation" class="active" id="settings_switch_config"><a href="#">Конфигурация</a></li>
                <li role="presentation"  id="settings_switch_storage"><a href="#">Отображение Storage</a></li>
                <li role="presentation" id="settings_switch_host"><a href="#">Отображение Host</a></li>
            </ul>
        </div>

        <div class="col-lg-9 col-md-9" style="height: 100%; overflow-y: scroll; padding-bottom: 30px;">
            <div id="settings_tab_config"  class="tab">
                <form id="settings_form_config">
                    <table id="settings_config" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Parameter</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </form>
                <button id="save_settings_config" class="btn btn-primary pull-right" style="width:150px;">Save</button>
            </div>


            <div id="settings_tab_storage"  class="tab">
                <form id="settings_form_storage">
                    <table id="settings_storage" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Alias</th>
                            <th>Display</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </form>
                <button id="save_settings_storage" class="btn btn-primary pull-right" style="width:150px;">Save</button>
            </div>


            <div id="settings_tab_host" class="tab">
                <form id="settings_form_host">
                    <table id="settings_host" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Host</th>
                            <th>Display</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </form>
                <button id="save_settings_host" class="btn btn-primary pull-right" style="width:150px;">Save</button>
            </div>

        </div>
    </div>

</div>

<script>

    //Удаляет префикс
    function removePrefix(elem, prefix) {
        return elem.replace(prefix, '');
    }

    function saveConfigSettings(settings) {
        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'saveConfigSettings',
                'data': settings
            },
            success:function(data){
                console.log(data);
                alert("Настройки сохранены!");
            }
        });
    }

    function saveStorageSettings(settings){
        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'saveStorageSettings',
                'data': settings
            },
            success:function(data){
                console.log(data);
                alert("Настройки сохранены!");
            }
        });
    }

    function saveHostSettings(settings){
        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'saveHostSettings',
                'data': settings
            },
            success:function(data){
                console.log(data);
                alert("Настройки сохранены!");
            }
        });
    }

    $(document).ready(function () {

        $(".tab").hide();
        $("#settings_tab_config").show();

        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'getSettings'
            },
            success:function(data){
                settings_list = JSON.parse(data);
                table_body = $("#settings_config > tbody");
                settings_list.forEach(function (item, i, array) {
                    table_body.append(
                        "<tr>" +
                            "<td>" + item.id + "</td>" +
                            "<td>" + item.parameter + "</td>" +
                            "<td><input name='" + item.id + "' type='text' class='form-control' value='" + item.value + "'></td>" +
                        "</tr>"
                    );
                });

            }
        });

        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'getStorageList'
            },
            success:function(data){
                list = JSON.parse(data);
                table_body = $("#settings_storage > tbody");
                list.forEach(function (item, i, array) {
                    if (item.display == 1){
                        checked = 'checked';
                    } else {
                        checked = '';
                    }
                    table_body.append(
                        "<tr>" +
                        "<td>" + item.id + "</td>" +
                        "<td>" + item.name + "</td>" +
                        "<td><input name='alias_" + item.id + "' type='text' class='form-control' value='" + item.alias + "'></td>" +
                        "<td><input name='display_" + item.id + "' type='checkbox' " + checked + "></td>" +
                        "</tr>"
                    );
                });

            }
        });

        $.ajax({
            type: 'POST',
            url: 'action.php',
            data: {
                'action': 'getHostList'
            },
            success:function(data){
                list = JSON.parse(data);
                table_body = $("#settings_host > tbody");
                list.forEach(function (item, i, array) {
                    if (item.display == 1){
                        checked = 'checked';
                    } else {
                        checked = '';
                    }
                    table_body.append(
                        "<tr>" +
                        "<td>" + item.id + "</td>" +
                        "<td>" + item.host + "</td>" +
                        "<td><input name='display_" + item.id + "' type='checkbox' " + checked + "></td>" +
                        "</tr>"
                    );
                });

            }
        });

        $("#swich_settings_list > li").on('click', function () {
           tab_id = removePrefix(this.id, "settings_switch_");
            $(".tab").hide();
            $("#settings_tab_" + tab_id).show();
            $(this).parent().find("li").removeClass("active");
            $(this).addClass("active");

        });

        $("#save_settings_config").on('click', function () {
            settings = JSON.stringify($('#settings_form_config').serializeArray());
            saveConfigSettings(settings);
        });

        $("#save_settings_storage").on('click', function () {
            form = $("#settings_form_storage").serializeArray();
            aliasesData = [];
            displayData = [];
            form.forEach(function (item) {
                if (item.name.indexOf("alias_") + 1){
                    storage_id = removePrefix(item.name, "alias_");
                    alias = item.value;
                    aliasesData.push({
                        'id': storage_id,
                        'alias': alias
                    });

                } else if (item.name.indexOf("display_") + 1){
                    storage_id = removePrefix(item.name, "display_");
                    if (item.value == "on"){
                        display = 1;
                    } else {
                        display = 0;
                    }
                    displayData.push({
                        'id': storage_id,
                        'display': display
                    });
                } else {
                    alert("Ошибка формы, перезагрузите страницу!");
                    return;
                }
            });
            sendingData = alasql("SELECT t1.id, t1.alias, t2.display FROM ? t1 LEFT JOIN ? t2 ON t1.id == t2.id", [aliasesData, displayData]);
            sendingData.forEach(function (item) {
                if (item.display == undefined){
                    item.display = 0;
                }
            });
            sendingData = JSON.stringify(sendingData);
            console.log(sendingData);
            saveStorageSettings(sendingData);
        });

        $("#save_settings_host").on('click', function () {
            form = $("#settings_form_host").serializeArray();
            displayData = [];
            form.forEach(function (item) {
                if (item.name.indexOf("display_") + 1){
                    host_id = removePrefix(item.name, "display_");
                    displayData.push({
                        'id': host_id,
                        'display': 1
                    });
                } else {
                    alert("Ошибка формы, перезагрузите страницу!");
                    return;
                }
            });

            sendingData = JSON.stringify(displayData);
            console.log(sendingData);
            saveHostSettings(sendingData);
        });





    });


</script>

</body>
</html>