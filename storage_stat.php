<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="shortcut icon" href="../mainSource/favicon.ico" type="image/x-icon" />

    <script src="../mainSource/jquery-2.2.4/jquery.min.js"></script>

    <script src="../mainSource/moment/moment-with-locales.min.js"></script>

    <script src="../mainSource/bootstrap-3.3.7/js/bootstrap.min.js"></script>

    <script src="../mainSource/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <link href="../mainSource/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="../mainSource/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/i18n/defaults-ru_RU.min.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.js"></script>

    <script src="../mainSource/winmarkltd-BootstrapFormHelpers-d4201db/js/bootstrap-formhelpers-phone.format.js"></script>

    <script src="../mainSource/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>

    <script language="javascript" type="text/javascript" src="scripts/alasql/console/alasql.min.js"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/amcharts.js" type="text/javascript"></script>

    <script src="scripts/amcharts_3.21.0/amcharts/serial.js" type="text/javascript"></script>

    <!--    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">-->

    <script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

    <script src="scripts/script.js"></script>

    <link rel="stylesheet" type="text/css" href="style/style.css">

    <title>
        RitmTools
    </title>

</head>
<body style="height: 100%;">

<a href="index" style="position: absolute; font-size: 30px; padding-left:4px; padding-right:8px; margin-top:15px; margin-left:10px" class="btn btn-default">
    <span class="glyphicon glyphicon-home" aria-hidden="true" style="width:35px;"></span>
</a>


<div class="container" style="height: 100%;">
    <div class="row" style="height: 10%; border-left:1px solid #ccc; border-right:1px solid #ccc;">

        <div class="col-lg-3 col-md-3" style="text-align:center;">
            <div class="input-group" style="width:70%; display: inline-block; margin-top:20px;">
                <select id="storageList" name="storage_id" class="form-control">
                </select>
            </div>
            <div class="input-group" style="width:20%; display: inline-block;">
                <button id="loadStorageInfo" class="form-control btn btn-default"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></button>
            </div>
        </div>

        <div class="col-lg-6 col-md-6" style="">
            <h2 style="text-align: center">Статистика используемого Клиентами места на DataStores</h2>
        </div>

        <div class="col-lg-3 col-md-3" style="text-align:center;"></div>

    </div>

    <div class="row" style="height: 90%;  border-left:1px solid #ccc; border-right:1px solid #ccc;">

        <div class="col-lg-12 col-md-12" style="height: 100%; overflow-y: hidden;">
            <div style="height: 100%; overflow-x: hidden; overflow-y: hidden;">

                <div class="loader loading"></div>
                <div style="max-widht:300px; position: absolute; z-index: 1; margin-left:70%; margin-top:0" class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span>
                    Используйте скроллбар для выбора периода
                </div>
                <div class="diagramm_container">
                    <!---      Сюда кидаем графики  --->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="scripts/storage_info.js"></script>

</body>
</html>